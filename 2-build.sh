# !/bin/bash
# debconf-get-selections | grep package_name
export RUN_STARTED=`date`

# IMPORTANT: MYADMINUSR MUST be set to the admin user installed during OS build
MYADMINUSR=walid
MYSQLROOTPASS=walid
MY_HOSTNAME=walid
MY_DOMAIN=dev
MY_ISPC_HOST=ispc
REAL_EMAIL=info@beget.io

MY_IPRANGE=10.10.0.0/16
MY_IP=10.10.0.50
MY_IPNETMASK=255.255.0.0
MY_IPNETWORK=10.10.0.0
MY_IPBROADCAST=10.10.255.255
MY_IPGATEWAY=10.10.0.1


# ORIGINAL_HOSTNAME should be set to your server's original hostname
# This is needed to maintain some localized name resolution on some servers.
# For example on rackspace, when you crate a VPS, dedicated, or CloudVPS server
# you're prompted for a hostname, use that value here. See /etc/hostname
ORIGINAL_HOSTNAME=walid


DRUSH_VERSION=7.x-5.7
DRUSH_MAKE_VERSION=6.x-2.3
PROVISION_VERSION=6.x-1.9
QUICKSTART_VERSION=7.x-1.x

DRUSH_GZ_URL=http://ftp.drupal.org/files/projects/drush-7.x-5.7.tar.gz
DRUSH_GZ_FILE=drush-7.x-5.7.tar.gz

FAIL2BAN_URL=http://olivier.sessink.nl/jailkit/jailkit-2.15.tar.gz
FAIL2BAN_FILE=jailkit-2.15.tar.gz

ISPC3_FILE_URL=http://www.ispconfig.org/downloads/ISPConfig-3-stable.tar.gz
ISPC3_FILE=ISPConfig-3-stable.tar.gz

# For full build, run all sections but post_install
RUN_SECTION_1=no # don't run if you ran prebuild, todo: remove section1
RUN_SECTION_2=yes
RUN_SECTION_3=yes
RUN_SECTION_4=yes
RUN_SECTION_5=yes
RUN_SECTION_6=yes
RUN_SECTION_7=yes
RUN_AEGIR_INST=no
RUN_ISPC_INST=yes
RUN_WEBMIN_INST=yes
# Do not run post install (eg. leave next variable at no)
RUN_POST_INSTALL=no

# ISPC_INST_MODE=auto or ISPC_INST_MODE=expert
# not yet implemented
ISPC_INST_MODE=auto

MY_FULLHOSTNAME=$MY_HOSTNAME.$MY_DOMAIN
MY_FULLISPCHOSTNAME=$MY_ISPC_HOST.$MY_FULLHOSTNAME

# set vars for later use
export HOST=$MY_HOSTNAME
export HOSTNAME=$MY_FULLHOSTNAME
export ISPCHOSTNAME=$MY_FULLISPCHOSTNAME


################### end variables declaration



echo "\$RUN_SECTION_1=$RUN_SECTION_1"
echo "\$RUN_SECTION_2=$RUN_SECTION_2"
echo "\$RUN_SECTION_3=$RUN_SECTION_3"
echo "\$RUN_SECTION_4=$RUN_SECTION_4"
echo "\$RUN_SECTION_5=$RUN_SECTION_5"
echo "\$RUN_SECTION_6=$RUN_SECTION_6"
echo "\$RUN_SECTION_7=$RUN_SECTION_7"

echo "\$RUN_AEGIR_INST=$RUN_AEGIR_INST"
echo "\$RUN_ISPC_INST=$RUN_ISPC_INST"
echo "\$ISPC_INST_MODE=$ISPC_INST_MODE"
echo "\$RUN_WEBMIN_INST=$RUN_WEBMIN_INST"

echo "\$ORIGINAL_HOSTNAME=$ORIGINAL_HOSTNAME"
echo "\$MYADMINUSR=$MYADMINUSR"
echo "\$MYSQLROOTPASS=$MYSQLROOTPASS"
echo "\$DRUSH_VERSION=$DRUSH_VERSION"
echo "\$DRUSH_MAKE_VERSION=$DRUSH_MAKE_VERSION"
echo "\$PROVISION_VERSION=$PROVISION_VERSION"
echo "\$QUICKSTART_VERSION=$QUICKSTART_VERSION"
echo "\$DRUSH_GZ_URL=$DRUSH_GZ_URL"
echo "\$DRUSH_GZ_FILE=$DRUSH_GZ_FILE"
echo "\$FAIL2BAN_URL=$FAIL2BAN_URL"
echo "\$FAIL2BAN_FILE=$FAIL2BAN_FILE"
echo "\$ISPC3_FILE_URL=$ISPC3_FILE_URL"
echo "\$ISPC3_FILE=$ISPC3_FILE"
echo "\$REAL_EMAIL=$REAL_EMAIL"
echo "\$MY_FULLHOSTNAME=$MY_FULLHOSTNAME"
echo "\$MY_FULLISPCHOSTNAME=$MY_FULLISPCHOSTNAME"
echo "\$MY_IPRANGE=$MY_IPRANGE"
echo "\$MY_IP=$MY_IP"
echo "\$HOST=$HOST"
echo "\$HOSTNAME=$HOSTNAME"
echo "\$ISPCHOSTNAME=ISPCHOSTNAME"

##################################################################
if [ "$RUN_SECTION_1" == "yes" ]; then 
##################################################################
# THESE EDITS, AND MORE, ARE DONE IN 1-prebuild.sh SO THERE'S NO NEED
# TO RUN SECTION 1. TODO: VALIDATE THEN REMOVE
cd ~/
sudo test -e section1buildlog.txt && sudo rm -f section1buildlog.txt
echo "started at: "`date` | tee section1buildlog.txt

# Bunch of stuff moved to 1-prebuild.sh used to go here

# make done marker/log
cd ~/
echo "done at: "`date` | tee -a section1buildlog.txt
##################################################################
fi
##################################################################


##################################################################
if [ "$RUN_SECTION_2" == "yes" ]; then 
##################################################################
cd ~/
sudo test -e section2buildlog.txt && sudo rm -f section2buildlog.txt
echo "started at: "`date` | tee section2buildlog.txt

# sync system clock
sudo apt-get -y install ntp ntpdate htop

# update, upgrade, reboot
sudo apt-get update
sudo apt-get -y upgrade

# Install Postfix, Courier, Saslauthd, MySQL, rkhunter, binutils
echo mysql-server mysql-server/root_password        password $MYSQLROOTPASS | sudo debconf-set-selections
echo mysql-server mysql-server/root_password_again  password $MYSQLROOTPASS | sudo debconf-set-selections        
echo postfix postfix/mynetworks      string  127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128 $MY_IPRANGE | sudo debconf-set-selections
echo postfix postfix/mailname        string  $HOSTNAME | sudo debconf-set-selections
echo postfix postfix/main_mailer_type        select  Internet Site | sudo debconf-set-selections
echo postfix postfix/destinations    string  $HOSTNAME, localhost.dev, , localhost | sudo debconf-set-selections
echo courier-base    courier-base/webadmin-configmode        boolean false | sudo debconf-set-selections
echo courier-ssl     courier-ssl/certnotice  note | sudo debconf-set-selections
sudo apt-get install -y postfix postfix-mysql postfix-doc mysql-client mysql-server courier-authdaemon courier-authlib-mysql courier-pop courier-pop-ssl courier-imap courier-imap-ssl libsasl2-2 libsasl2-modules libsasl2-modules-sql sasl2-bin libpam-mysql openssl getmail4 rkhunter binutils maildrop

echo "root:$REAL_EMAIL" | sudo tee -a /etc/aliases
sudo newaliases

# Install Amavisd-new, SpamAssassin, And Clamav
sudo apt-get -y install amavisd-new spamassassin clamav clamav-daemon zoo unzip bzip2 arj nomarch lzop cabextract apt-listchanges libnet-ldap-perl libauthen-sasl-perl clamav-docs daemon libio-string-perl libio-socket-ssl-perl libnet-ident-perl zip libnet-dns-perl
  
# Install Apache2, PHP5, phpMyAdmin, FCGI, suExec, Pear, And mcrypt
# MYSQLROOTPASS=walid
echo phpmyadmin       phpmyadmin/reconfigure-webserver  text     apache2    | sudo debconf-set-selections
echo phpmyadmin       phpmyadmin/dbconfig-install       boolean  false       | sudo debconf-set-selections
echo phpmyadmin       phpmyadmin/app-password-confirm   password $MYSQLROOTPASS | sudo debconf-set-selections
echo phpmyadmin       phpmyadmin/mysql/admin-pass       password $MYSQLROOTPASS | sudo debconf-set-selections
echo phpmyadmin       phpmyadmin/password-confirm       password $MYSQLROOTPASS | sudo debconf-set-selections
echo phpmyadmin       phpmyadmin/setup-password         password $MYSQLROOTPASS | sudo debconf-set-selections
echo phpmyadmin       phpmyadmin/mysql/app-pass         password $MYSQLROOTPASS | sudo debconf-set-selections
sudo apt-get -y install apache2 apache2.2-common apache2-doc apache2-mpm-prefork apache2-utils libexpat1 ssl-cert libapache2-mod-php5 php5 php5-common php5-gd php5-mysql php5-imap phpmyadmin php5-cli php5-cgi libapache2-mod-fcgid apache2-suexec php-pear php-auth php5-mcrypt mcrypt php5-imagick imagemagick libapache2-mod-suphp libopenssl-ruby libapache2-mod-ruby

# enable mods
sudo a2enmod suexec rewrite ssl actions include
sudo /etc/init.d/apache2 restart 
sudo a2enmod dav_fs dav auth_digest

# commented out so it forces ssl user under ispc url
sudo /etc/init.d/apache2 restart
     
# install bind dns server
sudo apt-get -y install bind9 dnsutils

#install additional files
sudo apt-get -y install apache2-threaded-dev libapache2-svn php5-dev php5-xsl php5-curl php5-pgsql php5-sqlite php5-xdebug php-apc

# Disable apparmor
sudo /etc/init.d/apparmor stop
sudo update-rc.d -f apparmor remove
sudo apt-get -y remove apparmor apparmor-utils

# fixed, post step 4 /etc/mysql/my.cnf
# mysql bind address still not commenting or adding federated
# unbind to 127.0.0.1 and start federated
#tmp comment: sudo sed -i 's/bind\-address/# bind\-address/g'                 /etc/mysql/my.cnf
#tmp comment: sudo sed -i 's/federated/# federated/g'                         /etc/mysql/my.cnf
#tmp comment: sudo sed -i 's/max_connections/# max_connections/g'             /etc/mysql/my.cnf
#tmp comment: sudo sed -i 's/max_user_connections/# max_user_connections/g'   /etc/mysql/my.cnf
#tmp comment: sudo sed -i 's/#log_slow_queries/log_slow_queries/g'            /etc/mysql/my.cnf
#tmp comment: sudo sed -i 's/#long_query_time/long_query_time/g'              /etc/mysql/my.cnf

#tmp comment: sudo sed -i 's/\[mysqld\]/\[mysqld\] \
#tmp comment: federated \
#tmp comment: max_connections = 500 \
#tmp comment: max_user_connections = 500 \
#tmp comment: /g' /etc/mysql/my.cnf
#tmp comment: # sudo nano /etc/mysql/my.cnf
sudo /etc/init.d/mysql restart

# optional, enable port 587 port submission and 465 smtps
sudo sed -i 's/#submission/submission/g' /etc/postfix/master.cf
sudo sed -i 's/#smtps/smtps/g' /etc/postfix/master.cf
sudo /etc/init.d/postfix restart

# make ssl folders
# sudo rm -r /etc/apache2/ssl.key/
# sudo rm -r /etc/apache2/ssl.csr/
# sudo rm -r /etc/apache2/ssl.crt/

sudo mkdir /etc/apache2/ssl.key/
sudo mkdir /etc/apache2/ssl.csr/
sudo mkdir /etc/apache2/ssl.crt/

# generate apache ssl files
sudo openssl genrsa -passout pass:0passphrase$MYSQLROOTPASS -des3 -out /etc/apache2/ssl.key/server.key 4096
#(echo US;echo IL;echo CHICAGO;echo SELF;echo APACHE SSL KEY;echo walid.dev;echo administrator@walid.dev;echo;echo;echo;echo;echo;echo;echo;echo;) |
sudo openssl req -passin pass:0passphrase$MYSQLROOTPASS -subj "/C=US/ST=IL/O=SELF/localityName=CHICAGO/commonName=$HOSTNAME/organizationalUnitName=TEST SSL KEY/emailAddress=administrator@$HOSTNAME/" -new -key /etc/apache2/ssl.key/server.key -out /etc/apache2/ssl.csr/server.csr
sudo openssl x509 -passin pass:0passphrase$MYSQLROOTPASS -req -days 3650 -in /etc/apache2/ssl.csr/server.csr -signkey /etc/apache2/ssl.key/server.key -out /etc/apache2/ssl.crt/server.crt
sudo openssl rsa -passin pass:0passphrase$MYSQLROOTPASS -in /etc/apache2/ssl.key/server.key -out /etc/apache2/ssl.key/server.key.insecure
sudo mv /etc/apache2/ssl.key/server.key /etc/apache2/ssl.key/server.key.secure
sudo mv /etc/apache2/ssl.key/server.key.insecure /etc/apache2/ssl.key/server.key
sudo a2enmod ssl

sudo /etc/init.d/mysql restart
sudo /etc/init.d/apache2 restart

# ################################################################################ Log Files
# LOGS=/home/walid/websites/logs
# Apache error logs are configured in the VirtualHosts section of each website (default from apache2.conf)
sudo touch     /var/log/apache2/error.log
sudo chmod g+w /var/log/apache2/error.log
ln -s          /var/log/apache2/error.log                /var/www/logs/apache-error.log
# This file catches any unconfigured log info for virtualhosts (default from apache2.conf)
sudo touch     /var/log/apache2/other_vhosts_access.log
sudo chmod g+w /var/log/apache2/other_vhosts_access.log
ln -s          /var/log/apache2/other_vhosts_access.log  /var/www/logs/apache-access.log
# php error logs are configured in php.ini  (changed in install-3-lamp.sh)
sudo touch     /var/log/php-error.log
sudo chmod g+w /var/log/php-error.log
ln -s          /var/log/php-error.log                    /var/www/logs/php-error.log
# MySQL error and slow query logs (changed in install-2-lamp.sh)
sudo touch     /var/log/mysql/error.log
sudo chmod g+w /var/log/mysql/error.log
ln -s          /var/log/mysql/error.log                  /var/www/logs/mysql-error.log
sudo touch     /var/log/mysql/mysql-slow.log
sudo chmod g+w /var/log/mysql/mysql-slow.log
ln -s          /var/log/mysql/mysql-slow.log             /var/www/logs/mysql-slow.log


sudo mkdir /var/www/backups
sudo mkdir /var/www/logs
# todo: sudo chown -cR $MYADMINUSR:$MYADMINUSR /var/www/backups /var/www/logs
sudo chown -cR $MYADMINUSR /var/www/backups /var/www/logs

sudo service mysql restart
sudo service apache2 graceful






# enable additional features on phpmyadmin
cd /usr/share/doc/phpmyadmin/examples/
sudo gunzip create_tables.sql.gz
sudo mysql --user=root --password=$MYSQLROOTPASS < create_tables.sql
cd ~/

# create phpmyadmin upload and save directories
sudo mkdir /usr/share/phpmyadmin/uploads/
sudo chmod 777 /usr/share/phpmyadmin/uploads/
sudo mkdir /usr/share/phpmyadmin/saves/
sudo chmod 777 /usr/share/phpmyadmin/saves/

sudo sed -i 's/\/\* Advance to next server for rest of config \*\//\/\* Advance to next server for rest of config \*\/\n\$cfg['\''Servers'\''][\$i]['\''tracking'\''] = '\''pma_tracking'\'';\n\$cfg['\''Servers'\''][\$i]['\''ForceSSL'\''] =  true;\n\$cfg['\''Servers'\''][\$i]['\''CountTables'\''] = true;\n\$cfg['\''Servers'\''][\$i]['\''MaxCharactersInDisplayedSQL'\''] = 2000;\n\$cfg['\''Servers'\''][\$i]['\''ExecTimeLimit'\''] = 300;\n\$cfg['\''Servers'\''][\$i]['\''ShowSQL'\''] = true;\n\$cfg['\''Servers'\''][\$i]['\''AllowUserDropDatabase'\''] =  true;\n\$cfg['\''Servers'\''][\$i]['\''Confirm'\''] = true;\n\$cfg['\''Servers'\''][\$i]['\''UseDbSearch'\''] = true;\n\$cfg['\''Servers'\''][\$i]['\''ShowStats'\''] = true;\n\$cfg['\''Servers'\''][\$i]['\''ShowPhpInfo'\''] =  true;\n\$cfg['\''Servers'\''][\$i]['\''ShowChgPassword'\''] = true;\n\$cfg['\''Servers'\''][\$i]['\''ShowCreateDb'\''] = true;\n\$cfg['\''Servers'\''][\$i]['\''ShowAll'\''] =  true;\n\$cfg['\''Servers'\''][\$i]['\''MaxRows'\''] = 500;\n\$cfg['\''Servers'\''][\$i]['\''DisplayBinaryAsHex'\''] = false;\n\$cfg['\''Servers'\''][\$i]['\''ProtectBinary'\''] = false;\n\$cfg['\''Servers'\''][\$i]['\''ZipDump'\''] = true;\n\$cfg['\''Servers'\''][\$i]['\''GZipDump'\''] = true;\n\$cfg['\''Servers'\''][\$i]['\''BZipDump'\''] = true;\n\$cfg['\''Servers'\''][\$i]['\''CompressOnFly'\''] = true;\n\$cfg['\''Servers'\''][\$i]['\''DefaultQueryTable'\''] = '\''SELECT \%f FROM \%t WHERE 1'\'';\$cfg['\''Servers'\''][\$i]['\''SQLQuery'\'']['\''ShowAsPHP'\''] = true;\n\$cfg['\''Servers'\''][\$i]['\''SQLQuery'\'']['\''Validate'\''] =  true;\n\$cfg['\''Servers'\''][\$i]['\''SQLQuery'\'']['\''Refresh'\''] = true;\n\$cfg['\''Servers'\''][\$i]['\''UploadDir'\''] = '\''\/usr\/share\/phpmyadmin\/uploads\/\%u\/'\'';\n\$cfg['\''Servers'\''][\$i]['\''SaveDir'\''] = '\''\/usr\/share\/phpmyadmin\/saves\/\%u\/'\''; \n\/\* Advance to next server for rest of config \*\//g' /etc/phpmyadmin/config.inc.php

#make phpmyadmin config edits
sudo tee -a /etc/phpmyadmin/config.inc.php <<EOFMARKER100
\$cfg['UploadDir'] = '/usr/share/phpmyadmin/uploads/%u/';
\$cfg['SaveDir'] = '/usr/share/phpmyadmin/saves/%u/';
\$_REQUEST['display_blob'] = true;
/*
* Prevent timeout for a year at a time.
* (seconds * minutes * hours * days * weeks)
* \$cfg['LoginCookieValidity'] = 60*60*24*7*52;
*/
\$cfg['LoginCookieValidity'] = 60*60;
ini_set('session.gc_maxlifetime', \$cfg['LoginCookieValidity']);
EOFMARKER100










# make phpmyadmin config edits
sudo tee -a /etc/phpmyadmin/config-db.php <<EOFMARKER101

\$dbuser='pma';
\$dbpass='$MYSQLROOTPASS';
\$dbname='phpmyadmin';
EOFMARKER101
    
# create phpmyadmin features user and grant privies on features db user
echo "CREATE USER 'pma'@'localhost' IDENTIFIED BY '$MYSQLROOTPASS'" | sudo mysql --user=root --password=$MYSQLROOTPASS
echo "GRANT ALL PRIVILEGES ON phpmyadmin.* TO 'pma'@'localhost' WITH GRANT OPTION" | sudo mysql --user=root --password=$MYSQLROOTPASS

# Disable apparmor
#tmp comment: sudo /etc/init.d/apparmor stop
#tmp comment: sudo update-rc.d -f apparmor remove
#tmp comment: sudo apt-get -y remove apparmor apparmor-utils

# restart apache
sudo /etc/init.d/mysql restart
sudo /etc/init.d/apache2 restart
    
# make done marker/log
cd ~/
echo "done at: "`date` | tee -a section2buildlog.txt
##################################################################
fi
##################################################################

##################################################################
if [ "$RUN_SECTION_3" == "yes" ]; then 
##################################################################
cd ~/
sudo test -e section3buildlog.txt && sudo rm -f section3buildlog.txt
# debconf-get-selections | grep postfix
echo "started at: "`date` | tee section3buildlog.txt

# drupal specific settings
sudo apt-get -y install python-software-properties subversion cvs git-core libevent-dev memcached curl libcurl3 libcurl3-dev postfix-pcre phppgadmin

# some additional ones for quickstart
sudo apt-get -y install bzr wget curl

# install imagemagick
sudo apt-get -y install imagemagick php5-imagick librmagick-ruby libmagickwand-dev

# boost limits
#tmp comment: sudo sed -i 's/max_execution_time = 30/max_execution_time = 120/' /etc/php5/apache2/php.ini
#tmp comment: sudo sed -i 's/max_execution_time = 30/max_execution_time = 120/' /etc/php5/cli/php.ini
#tmp comment: sudo sed -i 's/upload_max_filesize = 2M/upload_max_filesize = 32M/' /etc/php5/apache2/php.ini
#tmp comment: sudo sed -i 's/upload_max_filesize = 2M/upload_max_filesize = 32M/' /etc/php5/cli/php.ini
#tmp comment: sudo sed -i 's/post_max_size = 8M/post_max_size = 32M/' /etc/php5/apache2/php.ini
#tmp comment: sudo sed -i 's/post_max_size = 8M/post_max_size = 32M/' /etc/php5/cli/php.ini
#tmp comment: sudo sed -i 's/^error_reporting = .*/error_reporting = E_ALL \& ~E_DEPRECATED \& ~E_NOTICE/' /etc/php5/apache2/php.ini
#tmp comment: sudo sed -i 's/^error_reporting = .*/error_reporting = E_ALL \& ~E_DEPRECATED \& ~E_NOTICE/' /etc/php5/cli/php.ini
#tmp comment: sudo /etc/init.d/apache2 restart

# install pecl uploadprogress
sudo mkdir /tmp/pear
sudo mkdir /tmp/pear/cache
sudo pecl install uploadprogress
echo "extension = uploadprogress.so" | sudo tee /etc/php5/apache2/conf.d/uploadprogress.ini 
sudo /etc/init.d/apache2 reload

# postfix configs 
sudo postconf -e 'smtpd_sasl_local_domain ='
sudo postconf -e 'smtpd_sasl_auth_enable = yes'
sudo postconf -e 'smtpd_sasl_security_options = noanonymous'
sudo postconf -e 'broken_sasl_auth_clients = yes'
sudo postconf -e 'smtpd_sasl_authenticated_header = yes'
#tmp comment: sudo postconf -e 'smtpd_recipient_restrictions = permit_mynetworks, permit_sasl_authenticated, check_recipient_access mysql:/etc/postfix/mysql-virtual_recipient.cf, reject_rbl_client multi.uribl.com, reject_rbl_client zen.spamhaus.org, reject_rbl_client dnsbl.njabl.org, reject_rbl_client whois.rfc-ignorant.org, reject_rbl_client combined.rbl.msrbl.net, reject_unauth_destination'
sudo postconf -e 'inet_interfaces = all'
#nano /etc/postfix/main.cf



# make postfix ssl for tls
sudo mkdir /etc/postfix/ssl
cd /etc/postfix/ssl/
sudo openssl genrsa -des3 -rand /etc/hosts -passout pass:0passphrase$MYSQLROOTPASS -out smtpd.key 1024
chmod 600 smtpd.key
sudo openssl req -new -key smtpd.key -passin pass:0passphrase$MYSQLROOTPASS -subj "/C=US/ST=IL/O=SELF/localityName=CHICAGO/commonName=$HOSTNAME/organizationalUnitName=POSTFIX SSL KEY/emailAddress=administrator@$HOSTNAME/" -out smtpd.csr
sudo openssl x509 -req -days 3650 -in smtpd.csr -signkey smtpd.key -passin pass:0passphrase$MYSQLROOTPASS -out smtpd.crt
sudo openssl rsa -in smtpd.key -passin pass:0passphrase$MYSQLROOTPASS -out smtpd.key.unencrypted
sudo mv -f smtpd.key smtpd.key.encrypted
sudo mv -f smtpd.key.unencrypted smtpd.key
sudo openssl req -new -x509 -extensions v3_ca -keyout cakey.pem -passout pass:0passphrase$MYSQLROOTPASS -subj "/C=US/ST=IL/O=SELF/localityName=CHICAGO/commonName=$HOSTNAME/organizationalUnitName=CA SSL KEY/emailAddress=administrator@$HOSTNAME/" -out cacert.pem -days 3650
sudo postconf -e 'myhostname = '$HOSTNAME

# continue postfix edits
sudo postconf -e 'smtpd_tls_auth_only = no'
sudo postconf -e 'smtp_use_tls = yes'
sudo postconf -e 'smtpd_use_tls = yes'
sudo postconf -e 'smtp_tls_note_starttls_offer = yes'
sudo postconf -e 'smtpd_tls_key_file = /etc/postfix/ssl/smtpd.key' #/etc/postfix/ssl/smtpd.key
sudo postconf -e 'smtpd_tls_cert_file = /etc/postfix/ssl/smtpd.crt' #/etc/postfix/ssl/smtpd.crt
sudo postconf -e 'smtpd_tls_CAfile = /etc/postfix/ssl/cacert.pem'
sudo postconf -e 'smtpd_tls_loglevel = 1'
sudo postconf -e 'smtpd_tls_received_header = yes'
sudo postconf -e 'smtpd_tls_session_cache_timeout = 3600s'
sudo postconf -e 'tls_random_source = dev:/dev/urandom'
#nano /etc/postfix/main.cf
sudo mkdir -p /var/spool/postfix/var/run/saslauthd

sudo sed -i 's/START=/#START=/g' /etc/default/saslauthd
sudo sed -i '/MECH_OPTIONS/!s/OPTIONS=/#OPTIONS=/g' /etc/default/saslauthd
sudo tee -a /etc/default/saslauthd <<EOFMARKER210

    START=yes
    OPTIONS="-c -m /var/spool/postfix/var/run/saslauthd -r"
EOFMARKER210
# nano /etc/default/saslauthd

# sasl /etc/postfix/sasl/smtpd.conf
sudo service saslauthd restart
sudo service courier-pop restart
sudo service courier-imap restart
sudo service postfix restart
sudo sed -i '/pwcheck_method/d' /etc/postfix/sasl/smtpd.conf
sudo sed -i '/mech_list/d' /etc/postfix/sasl/smtpd.conf
sudo tee -a /etc/postfix/sasl/smtpd.conf <<EOFMARKER200

pwcheck_method: saslauthd
mech_list: plain login
EOFMARKER200

cd ~/
sudo adduser postfix sasl
sudo service saslauthd restart
sudo service courier-pop restart
sudo service courier-imap restart
sudo service postfix restart

# optional, set spamassassain to autoupdate via cron
echo "23 4 */2 * * /usr/bin/sa-update --no-gpg > /dev/null" | sudo tee -a /var/spool/cron/crontabs/root

# optional, set clamav to autoupdate via cron
sudo tee /usr/local/update_clamav.sh <<EOFMARKER12
#!/bin/bash
sudo /etc/init.d/clamav-freshclam stop
sudo freshclam -v
sudo /etc/init.d/clamav-freshclam start
EOFMARKER12
echo "15 00 * * * /usr/local/update_clamav.sh > /dev/null" | sudo tee -a /var/spool/cron/crontabs/root
    
# rc4 addition
sudo chmod 755 /usr/local/update_clamav.sh

# make done marker/log
cd ~/
echo "done at: "`date` | tee -a section3buildlog.txt

##################################################################
fi
##################################################################

##################################################################
if [ "$RUN_SECTION_4" == "yes" ]; then 
##################################################################
cd ~/
sudo test -e section4buildlog.txt && sudo rm -f section4buildlog.txt
echo "started at: "`date` | tee section4buildlog.txt

# generate mail ssls
#sudo rm -f /etc/courier/imapd.pem
#sudo rm -f /etc/courier/pop3d.pem
sudo test -e /etc/courier/imapd.pem && sudo rm -f /etc/courier/imapd.pem
sudo test -e /etc/courier/pop3d.pem && sudo rm -f /etc/courier/pop3d.pem



# remove and recreate imapd.cnf
# sudo rm -f /etc/courier/imapd.cnf
sudo test -e /etc/courier/imapd.cnf && sudo rm -f /etc/courier/imapd.cnf

sudo tee /etc/courier/imapd.cnf <<EOFMARKER1
RANDFILE = /usr/lib/courier/imapd.rand

[ req ]
default_bits = 2048
encrypt_key = yes
distinguished_name = req_dn
x509_extensions = cert_type
prompt = no

[ req_dn ]
C=US
ST=IL
L=CHICAGO
O=$HOST
OU=IMAP SSL KEY
CN=$HOSTNAME
emailAddress=administrator@$HOSTNAME

[ cert_type ]
nsCertType = server
EOFMARKER1

# remove and recreate pop3d.cnf
# sudo rm -f /etc/courier/pop3d.cnf
sudo test -e /etc/courier/pop3d.cnf && sudo rm -f /etc/courier/pop3d.cnf

sudo tee /etc/courier/pop3d.cnf <<EOFMARKER2
RANDFILE = /usr/lib/courier/pop3d.rand

[ req ]
default_bits = 2048
encrypt_key = yes
distinguished_name = req_dn
x509_extensions = cert_type
prompt = no

[ req_dn ]
C=US
ST=IL
L=CHICAGO
O=$HOST
OU=POP3 SSL KEY
CN=$HOSTNAME
emailAddress=administrator@$HOSTNAME

[ cert_type ]
nsCertType = server
EOFMARKER2

# make the certs
sudo mkimapdcert
sudo mkpop3dcert

# restart courier services
sudo /etc/init.d/courier-imap-ssl restart
sudo /etc/init.d/courier-pop-ssl restart

# install pureftpd and quota
sudo apt-get install -y pure-ftpd-common pure-ftpd-mysql quota quotatool

# (optional) make pureftp edits
sudo sed -i 's/STANDALONE_OR_INETD/\# STANDALONE_OR_INETD/g' /etc/default/pure-ftpd-common
sudo sed -i 's/VIRTUALCHROOT/\# VIRTUALCHROOT/g' /etc/default/pure-ftpd-common
echo "
    # $HOST edits: 
    STANDALONE_OR_INETD=standalone
    VIRTUALCHROOT=true
    " | sudo tee -a /etc/default/pure-ftpd-common

# (optional) allow ftp tls sessions 
sudo rm -f /etc/pure-ftpd/conf/TLS
echo 1 | sudo tee /etc/pure-ftpd/conf/TLS
# todo: setup a test to see if the directory already exists
sudo mkdir -p /etc/ssl/private/
# create cert
#(echo US; echo IL; echo CHICAGO; echo $HOST; echo PureFTP SSL Key; echo $HOSTNAME; echo administrator@$HOSTNAME; echo; echo;) | sudo openssl req -x509 -nodes -days 7300 -newkey rsa:2048 -keyout /etc/ssl/private/pure-ftpd.pem -out /etc/ssl/private/pure-ftpd.pem
sudo openssl req -x509 -subj "/C=US/ST=IL/O=SELF/localityName=CHICAGO/commonName=$HOSTNAME/organizationalUnitName=PureFTP SSL Key/emailAddress=administrator@$HOSTNAME/" -nodes -days 7300 -newkey rsa:2048 -keyout /etc/ssl/private/pure-ftpd.pem -out /etc/ssl/private/pure-ftpd.pem

# change the permissions of the ssl certificate:
# sudo chown root:ssl-cert /etc/ssl/private/pure-ftpd.pem
# sudo chmod 640 /etc/ssl/private/pure-ftpd.pem
sudo chmod 600 /etc/ssl/private/pure-ftpd.pem
# restart ftp server
sudo /etc/init.d/pure-ftpd-mysql restart


# install vlogger, webalizer, and awstats
sudo apt-get -y install vlogger webalizer awstats

# configure read permissions on apache log or awstats will throw errors
#tmp comment: sudo chmod 755 /var/log/apache2

# set default locale (if you don't do this, you'll get an error when cron runs that the
# /etc/default/locale file cannot be found. at a minimum, run touch /etc/default/locale)
sudo update-locale LANG=en_US.utf8 LC_ALL=en_US.utf8

# install fail2ban
sudo apt-get -y install fail2ban

# install jailkit
sudo apt-get -y install build-essential autoconf automake1.9 libtool flex bison debhelper
cd /tmp
sudo wget $FAIL2BAN_URL
sudo tar xvfz $FAIL2BAN_FILE
cd jailkit-2.15
sudo ./debian/rules binary
#cd ..
sudo dpkg -i jailkit_2.15-1_amd64.deb
cd ..
sudo dpkg -i jailkit_2.15-1_amd64.deb
sudo rm -rf /tmp/jailkit-2.15
cd ~/

sudo dpkg -i jailkit_2.15-1_amd64.deb
sudo rm -rf jailkit-2.15*
cd ~/

# install fail2ban and configure to use route instead of iptables for banning,
# (using route avoids conflicts with iptables - the firewall)
# see http://mattrude.com/projects/roundcube-fail2ban-plugin/

# remove and recreate fail2ban config file /etc/fail2ban/jail.local
sudo test -e /etc/fail2ban/jail.local && sudo cp -f /etc/fail2ban/jail.local /etc/fail2ban/jail.local.bak && sudo rm -f /etc/fail2ban/jail.local
sudo tee /etc/fail2ban/jail.local <<EOFMARKER3
# uses 50022, an alternate ssh port. to harden the server
# remove ssh and only use 50022 (but putty and) all other shortcuts/configs
# will need to be updated (i.e. drush alias files, etc.)
[DEFAULT]
banaction = route

[ssh]
enabled = true
port    = ssh,50022
filter  = sshd
logpath  = /var/log/auth.log
maxretry = 6

[pureftpd]
enabled  = true
port     = ftp,ftp-data,ftps,ftps-data
filter   = pureftpd
logpath  = /var/log/syslog
maxretry = 6
[sasl]
enabled  = true
port     = smtp,ssmtp
filter   = sasl
# You might consider monitoring /var/log/warn.log instead
# if you are running postfix. See http://bugs.debian.org/507990
logpath  = /var/log/mail.log
maxretry = 6

[courierpop3]
enabled  = true
port     = pop3
filter   = courierpop3
logpath  = /var/log/mail.log
maxretry = 5

[courierpop3s]
enabled  = true
port     = pop3s
filter   = courierpop3s
logpath  = /var/log/mail.log
maxretry = 5

[courierimap]
enabled  = true
port     = imap2,imap3
filter   = courierimap
logpath  = /var/log/mail.log
maxretry = 5

[courierimaps]
enabled  = true
port     = imaps
filter   = courierimaps
logpath  = /var/log/mail.log
maxretry = 5

# in my config, i have roundcube running under ispconfig site/sslcert so
# port 8080 is also needed. you can drop http if rouncube is only to be
# accessed from port 8080
[roundcube]
enabled  = true
port     = http,8080,8443
filter   = roundcube
logpath  = /var/log/roundcube/userlogins
maxretry = 5

# in my config, i have webmin running under port 50000 (not the default, 10000)
# you can drop either but not both 
[webmin-auth]
enabled = true
port    = 10000,50000
filter  = webmin-auth
logpath  = /var/log/auth.log
maxretry = 3
EOFMARKER3

# remove and recreate fail2ban config file /etc/fail2ban/action.d/route.conf
sudo test -e /etc/fail2ban/action.d/route.conf && sudo cp -f /etc/fail2ban/action.d/route.conf /etc/fail2ban/action.d/route.conf.bak && sudo rm -f /etc/fail2ban/action.d/route.conf
sudo tee /etc/fail2ban/action.d/route.conf <<EOFMARKER4
[Definition]
actionban = ip route add unreachable <ip>
actionunban = ip route del unreachable <ip>
EOFMARKER4

# remove and recreate fail2ban config file /etc/fail2ban/filter.d/pureftpd.conf
sudo test -e /etc/fail2ban/filter.d/pureftpd.conf && sudo cp -f /etc/fail2ban/filter.d/pureftpd.conf /etc/fail2ban/filter.d/pureftpd.conf.bak && sudo rm -f /etc/fail2ban/filter.d/pureftpd.conf
sudo tee /etc/fail2ban/filter.d/pureftpd.conf <<EOFMARKER5
[Definition]
failregex = .*pure-ftpd: \(.*@<HOST>\) \[WARNING\] Authentication failed for user.*
ignoreregex =
EOFMARKER5

# remove and recreate fail2ban config file /etc/fail2ban/filter.d/courierpop3.conf
sudo test -e /etc/fail2ban/filter.d/courierpop3.conf && sudo cp -f /etc/fail2ban/filter.d/courierpop3.conf /etc/fail2ban/filter.d/courierpop3.conf.bak && sudo rm -f /etc/fail2ban/filter.d/courierpop3.conf
sudo tee /etc/fail2ban/filter.d/courierpop3.conf <<EOFMARKER6
# Fail2Ban configuration file
#
# $Revision: 100 $
#
[Definition]
# Option:  failregex
# Notes.:  regex to match the password failures messages in the logfile. The
#          host must be matched by a group named \"host\". The tag \"<HOST>\" can
#          be used for standard IP/hostname matching and is only an alias for
#          (?:::f{4,6}:)?(?P<host>\S+)
# Values:  TEXT
#
failregex = pop3d: LOGIN FAILED.*ip=\[.*:<HOST>\]
# Option:  ignoreregex
# Notes.:  regex to ignore. If this regex matches, the line is ignored.
# Values:  TEXT
#
ignoreregex =
EOFMARKER6

# remove and recreate fail2ban config file /etc/fail2ban/filter.d/courierpop3s.conf
sudo test -e /etc/fail2ban/filter.d/courierpop3s.conf && sudo cp -f /etc/fail2ban/filter.d/courierpop3s.conf /etc/fail2ban/filter.d/courierpop3s.conf.bak && sudo rm -f /etc/fail2ban/filter.d/courierpop3s.conf
sudo tee /etc/fail2ban/filter.d/courierpop3s.conf <<EOFMARKER7
# Fail2Ban configuration file
#
# $Revision: 100 $
#
[Definition]
# Option:  failregex
# Notes.:  regex to match the password failures messages in the logfile. The
#          host must be matched by a group named \"host\". The tag \"<HOST>\" can
#          be used for standard IP/hostname matching and is only an alias for
#          (?:::f{4,6}:)?(?P<host>\S+)
# Values:  TEXT
#
failregex = pop3d-ssl: LOGIN FAILED.*ip=\[.*:<HOST>\]
# Option:  ignoreregex
# Notes.:  regex to ignore. If this regex matches, the line is ignored.
# Values:  TEXT
#
ignoreregex =
EOFMARKER7

# remove and recreate fail2ban config file /etc/fail2ban/filter.d/courierimap.conf
sudo test -e /etc/fail2ban/filter.d/courierimap.conf && sudo cp -f /etc/fail2ban/filter.d/courierimap.conf /etc/fail2ban/filter.d/courierimap.conf.bak && sudo rm -f /etc/fail2ban/filter.d/courierimap.conf
sudo tee /etc/fail2ban/filter.d/courierimap.conf <<EOFMARKER8
# Fail2Ban configuration file
#
# $Revision: 100 $
#
[Definition]
# Option:  failregex
# Notes.:  regex to match the password failures messages in the logfile. The
#          host must be matched by a group named \"host\". The tag \"<HOST>\" can
#          be used for standard IP/hostname matching and is only an alias for
#          (?:::f{4,6}:)?(?P<host>\S+)
# Values:  TEXT
#
failregex = imapd: LOGIN FAILED.*ip=\[.*:<HOST>\]
# Option:  ignoreregex
# Notes.:  regex to ignore. If this regex matches, the line is ignored.
# Values:  TEXT
#
ignoreregex =
EOFMARKER8

# remove and recreate fail2ban config file /etc/fail2ban/filter.d/courierimaps.conf
sudo test -e /etc/fail2ban/filter.d/courierimaps.conf && sudo cp -f /etc/fail2ban/filter.d/courierimaps.conf /etc/fail2ban/filter.d/courierimaps.conf.bak && sudo rm -f /etc/fail2ban/filter.d/courierimaps.conf
sudo tee /etc/fail2ban/filter.d/courierimaps.conf <<EOFMARKER9
# Fail2Ban configuration file
#
# $Revision: 100 $
#
[Definition]
# Option:  failregex
# Notes.:  regex to match the password failures messages in the logfile. The
#          host must be matched by a group named \"host\". The tag \"<HOST>\" can
#          be used for standard IP/hostname matching and is only an alias for
#          (?:::f{4,6}:)?(?P<host>\S+)
# Values:  TEXT
#
failregex = imapd-ssl: LOGIN FAILED.*ip=\[.*:<HOST>\]
# Option:  ignoreregex
# Notes.:  regex to ignore. If this regex matches, the line is ignored.
# Values:  TEXT
#
ignoreregex =
EOFMARKER9

# remove and recreate fail2ban config file /etc/fail2ban/filter.d/roundcube.conf
sudo test -e /etc/fail2ban/filter.d/roundcube.conf && sudo cp -f /etc/fail2ban/filter.d/roundcube.conf /etc/fail2ban/filter.d/roundcube.conf.bak && sudo rm -f /etc/fail2ban/filter.d/roundcube.conf
sudo tee /etc/fail2ban/filter.d/roundcube.conf <<EOFMARKER10
[Definition]
failregex = FAILED login for .*. from <HOST>
ignoreregex =
EOFMARKER10

# hack for DoS flood protection http://www.howtoforge.com/extending-perfect-server-debian-squeeze-ispconfig-3-p2
sudo sed -i 's/beautifier.setInputCmd(c)/time.sleep(0.05) # edit for DoS flood protection \
                        beautifier.setInputCmd(c) \
/g' /usr/bin/fail2ban-client

# restart fail2ban
sudo /etc/init.d/fail2ban restart

# make done marker/log
cd ~/
echo "done at: "`date` | tee -a section4buildlog.txt
##################################################################
fi
##################################################################



##################################################################
if [ "$RUN_SECTION_5" == "yes" ]; then 
##################################################################
cd ~/
sudo test -e section5buildlog.txt && sudo rm -f section5buildlog.txt
echo "started at: "`date` | tee section5buildlog.txt
# (optional) rackspace server doesn't have a user other than root,
# create user walid, default group walid, home dir /var/aegir aegir, uid 1000
#tmp comment: sudo adduser --home /home/$MYADMINUSR --shell /bin/bash --uid 1000 $MYADMINUSR
#tmp comment: sudo usermod -G adm --append $MYADMINUSR
#tmp comment: sudo usermod -G dialout --append $MYADMINUSR
#tmp comment: sudo usermod -G cdrom --append $MYADMINUSR
#tmp comment: sudo usermod -G sudo --append $MYADMINUSR
#tmp comment: sudo usermod -G www-data --append $MYADMINUSR
#tmp comment: sudo usermod -G plugdev --append $MYADMINUSR
#tmp comment: sudo usermod -G lpadmin --append $MYADMINUSR
#tmp comment: sudo usermod -G sambashare --append $MYADMINUSR
#tmp comment: sudo usermod -G admin --append $MYADMINUSR
#tmp comment: # sudo usermod -G aegir --append $MYADMINUSR
#tmp comment: # sudo usermod -G ispapps --append $MYADMINUSR
#tmp comment: # sudo usermod -G ispconfig --append $MYADMINUSR


# the following lines fix an error when running multitail file
# think there's a config about creating folders for users automatically or not, google "postfix config create user" 
# error: could not determine size of file /var/mail/root which is supposed to be your mailfile
sudo touch /var/mail/root
sudo chmod 600 /var/mail/root
sudo chown root /var/mail/root

# the following lines fix an error when running multitail file
# think there's a config about creating folders for users automatically or not, google "postfix config create user"
# error: could not determine size of file /var/mail/walid which is supposed to be your mailfile
sudo touch /var/mail/$MYADMINUSR
sudo chmod 600 /var/mail/$MYADMINUSR
sudo chown $MYADMINUSR /var/mail/$MYADMINUSR

# (optional) install multitail
sudo apt-get -y install multitail
# create monitoring script
sudo tee /root/mytail <<EOFMARKER11
#!/bin/bash
#multitail -ci yellow -e "ailed" -n 1000 /var/log/auth.log  \
#-ci red -e "Ban" -n 1000 -I /var/log/fail2ban.log \
#-ci red -e "fw" -n 1000 -I /var/log/messages \
#-ci green -e "Unban" -n 1000 -I /var/log/messages \
#-ci blue -e "fail" -n 1000 -I /var/log/syslog

#multitail -s 2 /var/log/auth.log /var/log/fail2ban.log /var/log/messages /var/log/syslog

multitail -s 2 -ci red /var/log/syslog \
-ci yellow /var/log/auth.log \
-ci magenta /var/log/fail2ban.log
EOFMARKER11

sudo tee /home/$MYADMINUSR/mytail <<EOFMARKER12
#!/bin/bash
#multitail -ci yellow -e "ailed" -n 1000 /var/log/auth.log  \
#-ci red -e "Ban" -n 1000 -I /var/log/fail2ban.log \
#-ci red -e "fw" -n 1000 -I /var/log/messages \
#-ci green -e "Unban" -n 1000 -I /var/log/messages \
#-ci blue -e "fail" -n 1000 -I /var/log/syslog

#multitail -s 2 /var/log/auth.log /var/log/fail2ban.log /var/log/messages /var/log/syslog

multitail -s 2 -ci red /var/log/syslog \
-ci yellow /var/log/auth.log \
-ci magenta /var/log/fail2ban.log
EOFMARKER12
#sudo chown $MYADMINUSR:$MYADMINUSR /home/$MYADMINUSR/mytail
sudo chown $MYADMINUSR /home/$MYADMINUSR/mytail


# remove first line, which starts with a comment, or apache will throw errors
sudo sed -i '/#/d' /etc/php5/cli/conf.d/imagick.ini
sudo sed -i '/#/d' /etc/php5/cli/conf.d/imap.ini
sudo sed -i '/#/d' /etc/php5/cli/conf.d/mcrypt.ini

# Install drush for common use (may have to do additionally for aegir)
# installed to /usr/local/src/ on servedrupal.com, below is correct for system wide access
cd /usr/local/lib
sudo git clone http://git.drupal.org/project/drush.git #or http://git.drupalcode.org/project/drush.git
cd /usr/local/lib/drush
sudo git checkout $DRUSH_VERSION
cd /usr/local/lib
sudo chmod u+x /usr/local/lib/drush/drush
sudo ln -s /usr/local/lib/drush/drush /usr/local/bin/drush

# Install drush make (only if using Drush 4.x or lower, if using Drush 5.x, 
# then DO NOT install and instead, comment out the next line
sudo mkdir /usr/local/lib/.drush
#cd /usr/local/lib/.drush
#sudo git clone http://git.drupalcode.org/project/drush_make.git
#cd /usr/local/lib/.drush/drush_make
#sudo git checkout $DRUSH_MAKE_VERSION
cd /usr/local/lib

# Install drush site install6 (only if using Drush 4.x or lower, if using Drush 5.x, 
# then DO NOT install and instead, comment out the next line
cd /usr/local/lib/.drush
#sudo git clone http://git.drupalcode.org/project/drush_site_install6.git
#cd /usr/local/lib/.drush/drush_site_install6
#sudo git checkout master
cd /usr/local/lib

# Install provision (aegir) (only if using Drush 4.x or lower, if using Drush 5.x, 
# then DO NOT install and instead, comment out the next line
cd /usr/local/lib/.drush
#sudo git clone http://git.drupalcode.org/project/provision.git
#cd /usr/local/lib/.drush/provision
#sudo git checkout $PROVISION_VERSION
cd /usr/local/lib

#tmp comment: # Install quickstart
#tmp comment: cd /usr/local/lib
#tmp comment: sudo git clone http://git.drupalcode.org/project/quickstart.git
#tmp comment: cd /usr/local/lib/quickstart
#tmp comment: sudo git checkout $QUICKSTART_VERSION
#tmp comment: cd /usr/local/lib

# Set perms
#sudo chown -cR $MYADMINUSR:$MYADMINUSR /usr/local/lib/drush /usr/local/lib/.drush /usr/local/lib/quickstart
sudo chown -cR $MYADMINUSR:drushusers /usr/local/lib/drush /usr/local/lib/.drush #tmp comment: /usr/local/lib/quickstart

#tmp comment: # Copy make file templates
#tmp comment: sudo cp /usr/local/lib/quickstart/make_templates/*.make /var/www

#tmp comment: # Install drush quickstart
#tmp comment: sudo ln -s /usr/local/lib/quickstart/drush /usr/local/lib/.drush/quickstart

# Create links
sudo ln -s /usr/local/lib/drush/drush /usr/local/bin/drush
sudo ln -s /usr/local/lib/drush/ /root/drush
sudo ln -s /usr/local/lib/.drush/ /root/.drush
ln -s /usr/local/lib/drush/ /home/$MYADMINUSR/drush
ln -s /usr/local/lib/.drush/ /home/$MYADMINUSR/.drush
#tmp comment: sudo ln -s /usr/local/lib/drush/ /home/quickstart/drush
#tmp comment: sudo ln -s /usr/local/lib/.drush/ /home/quickstart/.drush
# to do, script for all users (if needed, on production, dontt think that it is)

cd ~/

# end drush

##################################################################
if [ "$RUN_AEGIR_INST" == "yes" ]; then 
##################################################################
cd ~/
# (optional) install aegir
sudo adduser --system --group --home /var/aegir aegir
# make aegir a user of group www-data
sudo usermod -G www-data --append aegir
# if walid user exists, add to aegir group
sudo usermod -G aegir --append $MYADMINUSR 
echo "
    # $HOST edit for aegir:
    aegir ALL=NOPASSWD: /usr/sbin/apache2ctl
    " | sudo tee -a /etc/sudoers
#cd /var/aegir
#sudo wget http://ftp.drupal.org/files/projects/drush-7.x-4.4.tar.gz
#sudo gunzip -c drush-7.x-4.4.tar.gz | sudo tar -xf -
#sudo chown -cR aegir:aegir /var/aegir/drush
#sudo rm drush-7.x-4.4.tar.gz
# install drush in aegir folder
#sudo ln -s /var/aegir/drush/drush /usr/local/bin/drush
#cd ~/
#ln -s /var/aegir/drush drush
#
#(echo y;) | sudo drush dl --destination=/var/aegir/.drush provision-6.x
#sudo chown -cR aegir:aegir /var/aegir/.drush
#cd ~/
#ln -s /var/aegir/.drush .drush
sudo usermod -G aegir --append quickstart
sudo usermod -G aegir --append $MYADMINUSR
sudo ln -s /usr/local/lib/drush/ /var/aegir/drush
sudo ln -s /usr/local/lib/.drush/ /var/aegir/.drush
# end aegir
cd ~/
##################################################################
fi
##################################################################

# make done marker/log
cd ~/
echo "done at: "`date` | tee -a section5buildlog.txt
##################################################################
fi
##################################################################


##################################################################
if [ "$RUN_SECTION_6" == "yes" ]; then 
##################################################################
cd ~/
sudo test -e section6buildlog.txt && sudo rm -f section6buildlog.txt
echo "started at: "`date` | tee section6buildlog.txt

# install ISPConfig3
cd /tmp
wget $ISPC3_FILE_URL
tar xfz $ISPC3_FILE
cd ispconfig3_install/install/

sudo /etc/init.d/fail2ban restart
sudo /etc/init.d/saslauthd restart
sudo /etc/init.d/postfix restart
sudo /etc/init.d/mysql restart
sudo /etc/init.d/apache2 restart


##################################################################
if [ "$ISPC_INST_MODE" == "auto" ]; then 
##################################################################
(echo; echo; echo $ISPCHOSTNAME; echo; echo; echo $MYSQLROOTPASS; echo; echo; echo; echo; echo; echo; echo; echo; echo; echo; echo; echo; ) | sudo php -q install.php
##################################################################
fi
##################################################################


##################################################################
if [ "$ISPC_INST_MODE" == "expert" ]; then 
##################################################################
(echo; echo; echo $ISPCHOSTNAME; echo; echo; echo $MYSQLROOTPASS; echo; echo; echo; echo; echo; echo; echo; echo; echo; echo; echo; echo; ) | sudo php -q install.php
##################################################################
fi
##################################################################

# (optional) if walid user exists, add to ispc groups
# sudo usermod -G ispapps --append $HOST
# sudo usermod -G ispconfig --append $HOST
cd /tmp
sudo rm -rf /tmp/ispconfig3_install
sudo rm -f /tmp/$ISPC3_FILE

sudo /etc/init.d/apache2 restart

sudo sed -i 's/#SSLEngine On/SSLEngine On/g' /etc/apache2/sites-available/ispconfig.vhost
sudo sed -i 's/#SSLCertificateFile \/usr\/local\/ispconfig\/interface\/ssl\/ispserver.crt/SSLCertificateFile \/etc\/apache2\/ssl.crt\/server.crt/g' /etc/apache2/sites-available/ispconfig.vhost
sudo sed -i 's/#SSLCertificateKeyFile \/usr\/local\/ispconfig\/interface\/ssl\/ispserver.key/SSLCertificateKeyFile \/etc\/apache2\/ssl.key\/server.key/g' /etc/apache2/sites-available/ispconfig.vhost
sudo sed -i 's/Listen 8080/Listen 8443/g' /etc/apache2/sites-available/ispconfig.vhost
sudo sed -i 's/NameVirtualHost \*:8080/NameVirtualHost \*:8443/g' /etc/apache2/sites-available/ispconfig.vhost
sudo sed -i 's/VirtualHost _default_:8080/VirtualHost _default_:8443/g' /etc/apache2/sites-available/ispconfig.vhost
sudo sed -i 's/ServerAdmin webmaster@localhost/ServerName '$ISPCHOSTNAME' \
ServerAdmin administrator@'$HOSTNAME'/g' /etc/apache2/sites-available/ispconfig.vhost
sudo /etc/init.d/apache2 restart

sudo sed -i 's/Listen 8081/Listen 8444/g' /etc/apache2/sites-available/apps.vhost
sudo sed -i 's/# NameVirtualHost \*:8081/NameVirtualHost \*:8444/g' /etc/apache2/sites-available/apps.vhost
sudo sed -i 's/VirtualHost _default_:8081/VirtualHost _default_:8444/g' /etc/apache2/sites-available/apps.vhost
sudo sed -i 's/ServerAdmin webmaster@localhost/ServerAdmin '$ISPCHOSTNAME' \
ServerAdmin administrator@'$HOSTNAME'/g' /etc/apache2/sites-available/apps.vhost


cd ~/
# define add'l reserved user
echo "
aegir
drush
robert
administrator
admin
defconjuan
superuser
sa
dba
$MYADMINUSR" | sudo tee -a /usr/local/ispconfig/interface/lib/shelluser_blacklist

# delete all trailing blank lines at end of file
sudo sed -e :a -e '/^\n*$/{$d;N;ba' -e '}' /usr/local/ispconfig/interface/lib/shelluser_blacklist

# enable ssl on ISPC
sudo sed -i 's/<\/VirtualHost>/   SSLProtocol All -SSLv2 \
   SSLCipherSuite ALL:!EXP:!NULL:!ADH:!LOW \
   SetEnvIf User-Agent ..*MSIE.*. nokeepalive ssl-unclean-shutdown \
 \
   alias \/phpmyadmin \/usr\/share\/phpmyadmin \
   alias \/squirrelmail \/usr\/share\/squirrelmail \
   #alias \/roundcube \/var\/www\/clients\/client1\/web56\/web \
<\/VirtualHost> \
/g' /etc/apache2/sites-available/ispconfig.vhost

# link phpmyadmin
# sudo ln -s /usr/share/phpmyadmin/ /var/www/phpmyadmin
sudo ln -s /usr/share/phpmyadmin/ /usr/local/ispconfig/interface/web/phpmyadmin

# install squirrelmail
echo dictionaries-common     dictionaries-common/invalid_debconf_value   note | sudo debconf-set-selections
echo dictionaries-common     dictionaries-common/selecting_ispell_wordlist_default   note | sudo debconf-set-selections
# echo dictionaries-common     dictionaries-common/default-ispell      select  american (American English) | sudo debconf-set-selections
# echo dictionaries-common     dictionaries-common/default-wordlist    select  catala (Catalan) | sudo debconf-set-selections
# echo dictionaries-common     dictionaries-common/default-ispell      note | sudo debconf-set-selections
# echo dictionaries-common     dictionaries-common/default-wordlist    note | sudo debconf-set-selections
echo dictionaries-common     dictionaries-common/ispell-autobuildhash-message        note | sudo debconf-set-selections
echo dictionaries-common     dictionaries-common/old_wordlist_link   boolean true | sudo debconf-set-selections
echo dictionaries-common     dictionaries-common/move_old_usr_dict   boolean true | sudo debconf-set-selections
echo dictionaries-common     dictionaries-common/remove_old_usr_dict_link    boolean false | sudo debconf-set-selections
sudo apt-get -yq install squirrelmail
# next line commented out to force squirrelmail over ispc ssl
# sudo ln -s /usr/share/squirrelmail/ /var/www/webmail
sudo ln -s /usr/share/squirrelmail/ /usr/local/ispconfig/interface/web/webmail

# set webmail and phpmyadmin ssl redirects redirects
sudo sed -i 's/<\/VirtualHost>/    <IfModule mod_rewrite.c> \
     <\IfModule mod_ssl.c> \
      <Location \/phpmyadmin> \
       RewriteEngine on \
       RewriteCond \%{HTTPS} off \
       RewriteRule \^\(.\*\)\$ https:\/\/'$ISPCHOSTNAME':8443\/phpmyadmin [R] \
      <\/Location> \
      <Location \/webmail> \
       RewriteEngine on \
       RewriteCond \%{HTTPS} off \
       RewriteRule \^\(.\*\)\$ https:\/\/'$ISPCHOSTNAME':8443\/webmail [R] \
      <\/Location> \
     <\/IfModule> \
    <\/IfModule> \
<\/VirtualHost> \
/g' /etc/apache2/sites-available/default
     
/etc/init.d/apache2 restart
        

sudo /etc/init.d/apache2 restart

# fixed, post step 6
sudo sed -i 's/8081/8444/g' /etc/apache2/sites-available/apps.vhost
# sudo nano /etc/apache2/sites-available/apps.vhost

sudo /etc/init.d/apache2 restart

# fixed, post step 5
# fixes [warn] NameVirtualHost *:443 has no VirtualHosts on apache start
sudo sed -i 's/NameVirtualHost *:443/#NameVirtualHost *:443/g' /etc/apache2/ports.conf
sudo sed -i 's/Listen 443/#Listen 443/g' /etc/apache2/ports.conf
# sudo nano /etc/apache2/ports.conf
sudo /etc/init.d/apache2 restart
# end ISPCOnfig3


# fixed, post step 8 (fixed, works inline, skipped)
# add dns or hostname record for phpmyadmin.walid.dev, webmail.walid.dev, mail.walid.dev, www.walid.dev, cp.walid.dev, and ispc.walid.dev
# to point to local machine, then the following makes for pretty redirects
# (i.e. you won't have to enter port or directory to get to webadmin interfaces)
sudo mv /var/www/index.html /var/www/index.original.html
# sudo nano /var/www/index.php
sudo tee /var/www/index.php <<EOFMARKER421
<?php
if (\$_SERVER['HTTP_HOST'] == 'phpmyadmin.$HOSTNAME')
            {
            header('Location:https://$ISPCHOSTNAME:8443/phpmyadmin');
            }
if ((\$_SERVER['HTTP_HOST'] == 'webmail.$HOSTNAME') || (\$_SERVER['HTTP_HOST'] == 'mail.$HOSTNAME'))
            {
            header('Location:https://$ISPCHOSTNAME:8443/webmail');
            }
if ((\$_SERVER['HTTP_HOST'] == 'ispc.$HOSTNAME') || (\$_SERVER['HTTP_HOST'] == 'cp.$HOSTNAME'))
            {
            header('Location:https://$ISPCHOSTNAME:8443');
            }
if ((\$_SERVER['HTTP_HOST'] == 'www.$ISPCHOSTNAME') || (\$_SERVER['HTTP_HOST'] == '$HOSTNAME'))
            {
            header('Location:home.html');
            }
?>
EOFMARKER421
sudo /etc/init.d/apache2 restart

# fixes error: /usr/sbin/postconf: warning: /etc/postfix/master.cf: unused parameter: smtpd_bind_address=127.0.0.1
sudo sed -i 's/-o smtpd_bind_address=127.0.0.1/ /g' /etc/postfix/master.cf

# make done marker/log
cd ~/
echo "done at: "`date` | tee -a section6buildlog.txt
##################################################################
fi
##################################################################

##################################################################
if [ "$RUN_SECTION_7" == "yes" ]; then 
##################################################################
cd ~/
sudo test -e section7buildlog.txt && sudo rm -f section7buildlog.txt
echo "started at: "`date` | tee section7buildlog.txt
# optional, forward default system messages (email to root, postmaster, clamav, etc.)
# to a working email address
# echo "root:$REAL_EMAIL" | sudo tee -a /etc/aliases
# sudo newaliases

# install mod_evasive for DoS protection
sudo apt-get -y install libapache2-mod-evasive
sudo mkdir -p /var/log/apache2/evasive
sudo chown -R www-data:root /var/log/apache2/evasive
sudo rm -f /etc/apache2/mods-available/mod-evasive.load 
echo "LoadModule evasive20_module /usr/lib/apache2/modules/mod_evasive20.so
<IfModule  mod_evasive20.c>
    DOSHashTableSize 2048
    DOSPageCount 10
    DOSSiteCount 200
    DOSPageInterval 2
    DOSSiteInterval 2
    DOSBlockingPeriod 10
    DOSLogDir \"/var/log/apache2/evasive\"
    DOSEmailNotify administrator@$HOSTNAME
</IfModule>" | sudo tee /etc/apache2/mods-available/mod-evasive.load
sudo /etc/init.d/apache2 restart

# optional, show hidden files in ftp
sudo rm -f /etc/pure-ftpd/conf/DisplayDotFiles
echo "yes" | sudo tee /etc/pure-ftpd/conf/DisplayDotFiles

# optional, enable/set passive port range
sudo rm -f /etc/pure-ftpd/conf/PassivePortRange
echo "40110 40210" | sudo tee /etc/pure-ftpd/conf/PassivePortRange
#sudo /etc/init.d/pure-ftpd-mysql restart

# optional, increase pure-ftp default file list limit of 2,000 to 5,000
sudo rm -f /etc/pure-ftpd/conf/LimitRecursion
echo "5000 500" | sudo tee /etc/pure-ftpd/conf/LimitRecursion
sudo /etc/init.d/pure-ftpd-mysql restart

# if you want to host Ruby files with the extension .rb on your web sites created through ISPConfig, you must comment out the line application/x-ruby rb in /etc/mime.types:
sudo sed -i 's/application\/x-ruby/#application\/x-ruby /g' /etc/mime.types

# install php5xcache (do this or install eAccelerator or APC)
sudo apt-get install php5-xcache php-apc
sudo /etc/init.d/apache2 restart

sudo apt-get install libapache2-mod-fastcgi php5-fpm
sudo a2enmod actions fastcgi alias
sudo /etc/init.d/apache2 restart

##################################################################
if [ "$RUN_WEBMIN_INST" == "yes" ]; then 
##################################################################
# install webmin
echo "
# webmin repos:
deb http://download.webmin.com/download/repository sarge contrib
deb http://webmin.mirror.somersettechsolutions.co.uk/repository sarge contrib
" | sudo tee -a /etc/apt/sources.list

cd ~/
#cd /root
wget http://www.webmin.com/jcameron-key.asc
sudo apt-key add jcameron-key.asc
sudo apt-get update
sudo apt-get -y install webmin
sudo /etc/init.d/apache2 restart
# you can now access https://walid.dev:10000/
# optional: change port to 50000
sudo sed -i 's/port=10000/port=50000/g' /etc/webmin/miniserv.conf
sudo sed -i 's/listen=10000/listen=50000/g' /etc/webmin/miniserv.conf
sudo /etc/init.d/webmin restart
rm ~/jcameron-key.asc
# end webmin
cd ~/
##################################################################
fi
##################################################################

cd ~/
# based on which apps are installed, add the user walid to those groups
# no harm in leaving for gropus that don't exist
sudo usermod -G aegir --append $MYADMINUSR
sudo usermod -G www-data --append $MYADMINUSR
sudo usermod -G ispapps --append $MYADMINUSR
sudo usermod -G ispconfig --append $MYADMINUSR

#tmp comment: sudo usermod -G aegir --append quickstart
#tmp comment: sudo usermod -G www-data --append quickstart
#tmp comment: sudo usermod -G ispapps --append quickstart
#tmp comment: sudo usermod -G ispconfig --append quickstart

# make config links
mkdir /home/$MYADMINUSR/links/
sudo chown $MYADMINUSR:$MYADMINUSR /home/$MYADMINUSR/links
# main files
ln -s /etc/hosts /home/$MYADMINUSR/links/hosts
ln -s /etc/hostname /home/$MYADMINUSR/links/hostname
ln -s /etc/apt/sources.list /home/$MYADMINUSR/links/sources.list
ln -s /etc/ssh/sshd_config /home/$MYADMINUSR/links/sshd_config
ln -s /etc/fail2ban/ /home/$MYADMINUSR/links/fail2ban
# apache
ln -s /etc/apache2/ /home/$MYADMINUSR/links/apache
ln -s /etc/apache2/apache2.conf /home/$MYADMINUSR/links/apache2.conf
ln -s /etc/apache2/ports.conf /home/$MYADMINUSR/links/apache-ports.conf
ln -s /etc/apache2/mods-enabled/ /home/$MYADMINUSR/links/apache-mods-available
ln -s /etc/apache2/mods-available/ /home/$MYADMINUSR/links/apache-mods-available
ln -s /etc/apache2/sites-available/ /home/$MYADMINUSR/links/apache-sites-available
ln -s /etc/apache2/sites-enabled/ /home/$MYADMINUSR/links/apache-sites-enabled
ln -s /etc/apache2/conf.d/ /home/$MYADMINUSR/links/apache-conf.d
ln -s /etc/php5/apache2/php.ini /home/$MYADMINUSR/links/php-apache.ini
ln -s /etc/php5/cli/php.ini /home/$MYADMINUSR/links/php-cli.ini
ln -s /etc/sudoers /home/$MYADMINUSR/links/sudoers
ln -s /var/www/ /home/$MYADMINUSR/websites
# mysql
ln -s /etc/mysql/my.cnf /home/$MYADMINUSR/links/mysql.cnf
ln -s /etc/default/pure-ftpd-common /home/$MYADMINUSR/links/pure-ftpd-common
# phpmyadmin
ln -s /usr/share/phpmyadmin/config.inc.php /home/$MYADMINUSR/links/pma-default_config.inc.php
ln -s /etc/phpmyadmin/config-db.php /home/$MYADMINUSR/links/pma-my_config-db.php
ln -s /var/lib/phpmyadmin/blowfish_secret.inc.php /home/$MYADMINUSR/links/pma-my_blowfish_secret.inc.php
ln -s /etc/phpmyadmin/config.inc.php /home/$MYADMINUSR/links/pma-my_config.inc.php
# ispc
ln -s /usr/local/ispconfig/server/lib/mysql_clientdb.conf /home/$MYADMINUSR/links/ispc-server-dbconnection.conf
ln -s /usr/local/ispconfig/server/ /home/$MYADMINUSR/links/ispc-server
ln -s /usr/local/ispconfig/server/conf/ /home/$MYADMINUSR/links/ispc-server-conf
ln -s /usr/local/ispconfig/server/conf/index/ /home/$MYADMINUSR/links/ispc-templates-index
ln -s /usr/local/ispconfig/server/conf/error/ /home/$MYADMINUSR/links/ispc-templates-error
ln -s /usr/local/ispconfig/interface/lib/config.inc.php /home/$MYADMINUSR/links/ispc-config.inc.php
ln -s /usr/local/ispconfig/interface/lib/shelluser_blacklist /home/$MYADMINUSR/links/ispc-shelluser_blacklist
ln -s /var/www/clients /home/$MYADMINUSR/links/ispc-clients 
# postfix
ln -s /etc/postfix/master.cf /home/$MYADMINUSR/links/postfix-master.cf
ln -s /etc/postfix/mysql-virtual_sender.cf /home/$MYADMINUSR/links/postfix-mysql-virtual_sender.cf
ln -s /etc/postfix/mysql-virtual_client.cf /home/$MYADMINUSR/links/postfix-mysql-virtual_client.cf
ln -s /etc/postfix/main.cf /home/$MYADMINUSR/links/postfix-main.cf
# other webadmin interfaces
ln -s /usr/share/squirrelmail/ /home/$MYADMINUSR/links/webadmin-squirrelmail
ln -s /etc/webmin/ /home/$MYADMINUSR/links/webadmin-webmin
ln -s /usr/share/phpmyadmin/ /home/$MYADMINUSR/links/webadmin-phpmyadmin
ln -s /usr/local/ispconfig/interface/web /home/$MYADMINUSR/links/webadmin-ispc
# clam and spam assasain
ln -s /usr/local/update_clamav.sh /home/$MYADMINUSR/links/update_clamav.sh
ln -s /usr/bin/sa-update /home/$MYADMINUSR/links/sa-update
# aegir specific
ln -s /etc/apache2/conf.d/aegir.conf /home/$MYADMINUSR/links/aegir.conf
ln -s /var/aegir/backups /home/$MYADMINUSR/links/aegir-backups
ln -s /var/aegir/clients /home/$MYADMINUSR/links/aegir-clients
ln -s /var/aegir/config /home/$MYADMINUSR/links/aegir-config
ln -s /var/aegir/platforms /home/$MYADMINUSR/links/aegir-platforms
ln -s /var/aegir/hostmaster-6.x-1.1 /home/$MYADMINUSR/links/aegir-hostmaster-6.x-1.1

# make config links for root
mkdir /root/links/
sudo chown root:root /root/links
# main files
ln -s /etc/hosts /root/links/hosts
ln -s /etc/hostname /root/links/hostname
ln -s /etc/apt/sources.list /root/links/sources.list
ln -s /etc/ssh/sshd_config /root/links/sshd_config
ln -s /etc/fail2ban/ /root/links/fail2ban
# apache
ln -s /etc/apache2/ /root/links/apache
ln -s /etc/apache2/apache2.conf /root/links/apache2.conf
ln -s /etc/apache2/ports.conf /root/links/apache-ports.conf
ln -s /etc/apache2/mods-enabled/ /root/links/apache-mods-available
ln -s /etc/apache2/mods-available/ /root/links/apache-mods-available
ln -s /etc/apache2/sites-available/ /root/links/apache-sites-available
ln -s /etc/apache2/sites-enabled/ /root/links/apache-sites-enabled
ln -s /etc/apache2/conf.d/ /root/links/apache-conf.d
ln -s /etc/php5/apache2/php.ini /root/links/php-apache.ini
ln -s /etc/php5/cli/php.ini /root/links/php-cli.ini
ln -s /etc/sudoers /root/links/sudoers
ln -s /var/www/ /root/websites
# mysql
ln -s /etc/mysql/my.cnf /root/links/mysql.cnf
ln -s /etc/default/pure-ftpd-common /root/links/pure-ftpd-common
# phpmyadmin
ln -s /usr/share/phpmyadmin/config.inc.php /root/links/pma-default_config.inc.php
ln -s /etc/phpmyadmin/config-db.php /root/links/pma-my_config-db.php
ln -s /var/lib/phpmyadmin/blowfish_secret.inc.php /root/links/pma-my_blowfish_secret.inc.php
ln -s /etc/phpmyadmin/config.inc.php /root/links/pma-my_config.inc.php
# ispc
ln -s /usr/local/ispconfig/server/lib/mysql_clientdb.conf /root/links/ispc-server-dbconnection.conf
ln -s /usr/local/ispconfig/server/ /root/links/ispc-server
ln -s /usr/local/ispconfig/server/conf/ /root/links/ispc-server-conf
ln -s /usr/local/ispconfig/server/conf/index/ /root/links/ispc-templates-index
ln -s /usr/local/ispconfig/server/conf/error/ /root/links/ispc-templates-error
ln -s /usr/local/ispconfig/interface/lib/config.inc.php /root/links/ispc-config.inc.php
ln -s /usr/local/ispconfig/interface/lib/shelluser_blacklist /root/links/ispc-shelluser_blacklist
ln -s /var/www/clients /root/links/ispc-clients 
# postfix
ln -s /etc/postfix/master.cf /root/links/postfix-master.cf
ln -s /etc/postfix/mysql-virtual_sender.cf /root/links/postfix-mysql-virtual_sender.cf
ln -s /etc/postfix/mysql-virtual_client.cf /root/links/postfix-mysql-virtual_client.cf
ln -s /etc/postfix/main.cf /root/links/postfix-main.cf
# other webadmin interfaces
ln -s /usr/share/squirrelmail/ /root/links/webadmin-squirrelmail
ln -s /etc/webmin/ /root/links/webadmin-webmin
ln -s /usr/share/phpmyadmin/ /root/links/webadmin-phpmyadmin
ln -s /usr/local/ispconfig/interface/web /root/links/webadmin-ispc
# clam and spam assasain
ln -s /usr/local/update_clamav.sh /root/links/update_clamav.sh
ln -s /usr/bin/sa-update /root/links/sa-update
# aegir specific
ln -s /etc/apache2/conf.d/aegir.conf /root/links/aegir.conf
ln -s /var/aegir/backups /root/links/aegir-backups
ln -s /var/aegir/clients /root/links/aegir-clients
ln -s /var/aegir/config /root/links/aegir-config
ln -s /var/aegir/platforms /root/links/aegir-platforms
ln -s /var/aegir/hostmaster-6.x-1.1 /root/links/aegir-hostmaster-6.x-1.1



# fixed, post step 1 (fixed, works inline, skip)
sudo /etc/init.d/postfix restart
sudo /etc/init.d/saslauthd restart
sudo postconf -e 'smtpd_sasl_local_domain ='
sudo postconf -e 'smtpd_sasl_auth_enable = yes'
sudo postconf -e 'smtpd_sasl_security_options = noanonymous'
sudo postconf -e 'broken_sasl_auth_clients = yes'
sudo postconf -e 'smtpd_sasl_authenticated_header = yes'
sudo postconf -e 'smtpd_recipient_restrictions = permit_mynetworks, permit_sasl_authenticated, check_recipient_access mysql:/etc/postfix/mysql-virtual_recipient.cf, reject_rbl_client multi.uribl.com, reject_rbl_client zen.spamhaus.org, reject_rbl_client dnsbl.njabl.org, reject_rbl_client whois.rfc-ignorant.org, reject_rbl_client combined.rbl.msrbl.net, reject_unauth_destination'
sudo postconf -e 'smtpd_tls_auth_only = no'
sudo postconf -e 'smtp_use_tls = yes'
sudo postconf -e 'smtpd_use_tls = yes'
sudo postconf -e 'smtp_tls_note_starttls_offer = yes'
sudo postconf -e 'smtpd_tls_key_file = /etc/postfix/ssl/smtpd.key' # /etc/postfix/ssl/smtpd.key
sudo postconf -e 'smtpd_tls_cert_file = /etc/postfix/ssl/smtpd.crt' # /etc/postfix/ssl/smtpd.crt
sudo postconf -e 'smtpd_tls_CAfile = /etc/postfix/ssl/cacert.pem'
sudo postconf -e 'smtpd_tls_loglevel = 1'
sudo postconf -e 'smtpd_tls_received_header = yes'
sudo postconf -e 'smtpd_tls_session_cache_timeout = 3600s'
sudo postconf -e 'tls_random_source = dev:/dev/urandom'
sudo postconf -e 'mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128 '$MY_IPRANGE
sudo postconf -e 'myhostname = '$HOSTNAME
sudo postconf -e 'mydestination = '$HOSTNAME', '$ISPCHOSTNAME', localhost.localdomain, localhost'
# nano /etc/postfix/sasl/smtpd.conf
# nano /etc/postfix/main.cf
sudo /etc/init.d/postfix restart
sudo /etc/init.d/saslauthd restart
# sudo nano /etc/postfix/sasl/smtpd.conf
# sudo nano /etc/postfix/main.cf



# make done marker/log
cd ~/
echo "done at: "`date` | tee -a section7buildlog.txt
##################################################################
fi
##################################################################



##################################################################
if [ "$RUN_POST_INSTALL" == "yes" ]; then 
##################################################################
# fixed, post step 2 /etc/resolv.conf (doesn't work inline, must run post | validated)
sudo sed -i '/domain/d' /etc/resolv.conf
sudo sed -i '/search/d' /etc/resolv.conf
#sudo /etc/init.d/networking restart
# sudo nano /etc/resolv.conf

##################################################################
if [ "$NEVER_RUN" == "sdfasdfaissdfasdf" ]; then 
##################################################################
# fixed, post step 1 (fixed, works inline, skipped)
sudo /etc/init.d/postfix restart
sudo /etc/init.d/saslauthd restart
sudo postconf -e 'smtpd_sasl_local_domain ='
sudo postconf -e 'smtpd_sasl_auth_enable = yes'
sudo postconf -e 'smtpd_sasl_security_options = noanonymous'
sudo postconf -e 'broken_sasl_auth_clients = yes'
sudo postconf -e 'smtpd_sasl_authenticated_header = yes'
sudo postconf -e 'smtpd_recipient_restrictions = permit_mynetworks, permit_sasl_authenticated, check_recipient_access mysql:/etc/postfix/mysql-virtual_recipient.cf, reject_rbl_client multi.uribl.com, reject_rbl_client zen.spamhaus.org, reject_rbl_client dnsbl.njabl.org, reject_rbl_client whois.rfc-ignorant.org, reject_rbl_client combined.rbl.msrbl.net, reject_unauth_destination'
sudo postconf -e 'smtpd_tls_auth_only = no'
sudo postconf -e 'smtp_use_tls = yes'
sudo postconf -e 'smtpd_use_tls = yes'
sudo postconf -e 'smtp_tls_note_starttls_offer = yes'
sudo postconf -e 'smtpd_tls_key_file = /etc/postfix/ssl/smtpd.key'
sudo postconf -e 'smtpd_tls_cert_file = /etc/postfix/ssl/smtpd.crt'
sudo postconf -e 'smtpd_tls_CAfile = /etc/postfix/ssl/cacert.pem'
sudo postconf -e 'smtpd_tls_loglevel = 1'
sudo postconf -e 'smtpd_tls_received_header = yes'
sudo postconf -e 'smtpd_tls_session_cache_timeout = 3600s'
sudo postconf -e 'tls_random_source = dev:/dev/urandom'
sudo postconf -e 'mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128 '$MY_IPRANGE
sudo postconf -e 'myhostname = '$HOSTNAME
sudo postconf -e 'mydestination = '$HOSTNAME', '$ISPCHOSTNAME', localhost.localdomain, localhost'
# sudo nano /etc/postfix/sasl/smtpd.conf
# sudo nano /etc/postfix/main.cf
sudo /etc/init.d/postfix restart
sudo /etc/init.d/saslauthd restart
# sudo nano /etc/postfix/sasl/smtpd.conf
# sudo nano /etc/postfix/main.cf

# fixed, post step 3 /etc/hosts (fixed, works inline, skipped)
# 127.0.0.1 points to walid.dev, should be localhost.localdomain localhost
sudo sed -i '/127\.0\.0\.1/d' /etc/hosts
sudo sed -i '/127\.0\.1\.1/d' /etc/hosts
sudo sed -i '/'$MY_IP'/d' /etc/hosts
sudo tee -a /etc/hosts <<EOFMARKER1600
    127.0.0.1 localhost.localdomain localhost
    $MY_IP $MY_FULLHOSTNAME $MY_FULLISPCHOSTNAME $MY_HOSTNAME
EOFMARKER1600
sudo /etc/init.d/networking restart
# sudo nano /etc/hosts

# fixed, post step 4 /etc/mysql/my.cnf (fixed, works inline, skipped)
# mysql bind address still not commenting or adding federated
# unbind to 127.0.0.1 and start federated
sudo sed -i 's/bind\-address/\# bind\-address/g' /etc/mysql/my.cnf
sudo sed -i 's/federated/#federated/g' /etc/mysql/my.cnf
sudo sed -i 's/max_connections/#max_connections/g' /etc/mysql/my.cnf
sudo sed -i 's/max_user_connections/#max_user_connections/g' /etc/mysql/my.cnf
sudo sed -i 's/\[mysqld\]/\[mysqld\] \
federated \
max_connections = 500 \
max_user_connections = 500 \
/g' /etc/mysql/my.cnf
# sudo nano /etc/mysql/my.cnf
sudo /etc/init.d/mysql restart

# fixed, post step 5 (fixed, works inline, skipped)
# fixes [warn] NameVirtualHost *:443 has no VirtualHosts on apache start
sudo sed -i 's/NameVirtualHost *:443/#NameVirtualHost *:443/g' /etc/apache2/ports.conf
sudo sed -i 's/Listen 443/#Listen 443/g' /etc/apache2/ports.conf
# sudo nano /etc/apache2/ports.conf

# fixed, post step 6 (fixed, works inline, skipped)
sudo sed -i 's/8081/8444/g' /etc/apache2/sites-available/apps.vhost
# sudo nano /etc/apache2/sites-available/apps.vhost

# fixed, post step 7 (fixed, works inline, skipped)
# the following lines fix an error when running multitail file
# think there's a config about creating folders for users automatically or not, google "postfix config create user" 
# error: could not determine size of file /var/mail/root which is supposed to be your mailfile
sudo touch /var/mail/root
sudo chmod 600 /var/mail/root
sudo chown root /var/mail/root
# the following lines fix an error when running multitail file
# think there's a config about creating folders for users automatically or not, google "postfix config create user"
# error: could not determine size of file /var/mail/walid which is supposed to be your mailfile
sudo touch /var/mail/walid
sudo chmod 600 /var/mail/walid
sudo chown walid /var/mail/walid

# fixed, post step 8 (fixed, works inline, skipped)
# add dns or hostname record for phpmyadmin.walid.dev, webmail.walid.dev, mail.walid.dev, www.walid.dev, cp.walid.dev, and ispc.walid.dev
# to point to local machine, then the following makes for pretty redirects
# (i.e. you won't have to enter port or directory to get to webadmin interfaces)
sudo mv /var/www/index.html /var/www/index.original.html
# sudo nano /var/www/index.php
sudo tee /var/www/index.php <<EOFMARKER421
<?php
if (\$_SERVER['HTTP_HOST'] == 'phpmyadmin.$HOSTNAME')
            {
            header('Location:https://$ISPCHOSTNAME:8443/phpmyadmin');
            }
if ((\$_SERVER['HTTP_HOST'] == 'webmail.$HOSTNAME') || (\$_SERVER['HTTP_HOST'] == 'mail.$HOSTNAME'))
            {
            header('Location:https://$ISPCHOSTNAME:8443/webmail');
            }
if ((\$_SERVER['HTTP_HOST'] == 'ispc.$HOSTNAME') || (\$_SERVER['HTTP_HOST'] == 'cp.$HOSTNAME'))
            {
            header('Location:https://$ISPCHOSTNAME:8443');
            }
if (\$_SERVER['HTTP_HOST'] == '$HOSTNAME')
            {
            header('Location:http://www.$HOSTNAME');
            }
?>
EOFMARKER421
sudo /etc/init.d/apache2 restart

##################################################################
fi
##################################################################


##################################################################
fi
##################################################################


##################################################################
if [ "$NEVER_RUN" == "sdfasdfaissdfasdf" ]; then 
##################################################################
# System > Server Config > Web > Apps-vhost port: 8444
# add path to open_dirs for imagemagick (or imagemagick won't work
# in ispc > System > Server Config > Web (essential for imagemagick in drupal)
    # Append :/usr/bin/convert to PHP open_basedir
    
# to do:
# install roundcube
# make follow-up readme from my other build notes
# turn off ssh 22, leave 50022
# to fix:
# section 3, done not echoed
# section 4, start not echoed
# section 5, done not echoed
# section 6, start not echoed
# section 7, all good


sudo nano /etc/sudoers
sudo nano /etc/network/interfaces
sudo nano /etc/resolv.conf
sudo nano /etc/hosts
sudo nano /etc/hostname
sudo nano /etc/ssh/sshd_config
sudo nano /etc/aliases
sudo nano /etc/mysql/my.cnf
sudo nano /etc/postfix/master.cf
sudo nano /etc/phpmyadmin/config-db.php
sudo nano /etc/php5/apache2/conf.d/uploadprogress.ini
sudo nano /etc/postfix/sasl/smtpd.conf
sudo nano /etc/default/saslauthd
sudo nano /etc/courier/imapd.cnf
sudo nano /etc/courier/pop3d.cnf
sudo nano /etc/default/pure-ftpd-common
sudo nano /etc/fail2ban/jail.local
sudo nano /etc/fail2ban/action.d/route.conf
sudo nano /etc/fail2ban/filter.d/pureftpd.conf
sudo nano /etc/fail2ban/filter.d/courierpop3.conf
sudo nano /etc/fail2ban/filter.d/courierpop3s.conf
sudo nano /etc/fail2ban/filter.d/courierimap.conf
sudo nano /etc/fail2ban/filter.d/courierimaps.conf
sudo nano /etc/fail2ban/filter.d/roundcube.conf
sudo nano /usr/bin/fail2ban-client
sudo nano /usr/local/ispconfig/interface/lib/shelluser_blacklist
sudo nano /etc/apache2/sites-available/ispconfig.vhost

sudo nano /etc/apache2/sites-available/apps.vhost
sudo nano /etc/apache2/sites-available/default
sudo nano /etc/apache2/sites-available/ispconfig.conf
sudo nano /etc/apache2/sites-available/ispconfig.vhost
sudo nano /etc/apache2/apache2.conf
sudo nano /etc/apache2/ports.conf
sudo nano /etc/webmin/miniserv.conf
sudo nano /home/walid/section1buildlog.txt
sudo nano /home/walid/section2buildlog.txt
sudo nano /home/walid/section3buildlog.txt
sudo nano /home/walid/section4buildlog.txt
sudo nano /home/walid/section5buildlog.txt
sudo nano /home/walid/section6buildlog.txt
sudo nano /home/walid/section7buildlog.txt

sudo nano /etc/sudoers /etc/network/interfaces /etc/resolv.conf /etc/hosts /etc/hostname /etc/ssh/sshd_config /etc/aliases /etc/mysql/my.cnf /etc/postfix/master.cf /etc/phpmyadmin/config-db.php /etc/php5/apache2/conf.d/uploadprogress.ini /etc/postfix/sasl/smtpd.conf /etc/default/saslauthd /etc/courier/imapd.cnf /etc/courier/pop3d.cnf /etc/default/pure-ftpd-common /etc/fail2ban/jail.local /etc/fail2ban/action.d/route.conf /etc/fail2ban/filter.d/pureftpd.conf /etc/fail2ban/filter.d/courierpop3.conf /etc/fail2ban/filter.d/courierpop3s.conf /etc/fail2ban/filter.d/courierimap.conf /etc/fail2ban/filter.d/courierimaps.conf /etc/fail2ban/filter.d/roundcube.conf /usr/bin/fail2ban-client /usr/local/ispconfig/interface/lib/shelluser_blacklist /etc/apache2/sites-available/ispconfig.vhost /etc/webmin/miniserv.conf /home/walid/section1buildlog.txt /home/walid/section2buildlog.txt /home/walid/section3buildlog.txt /home/walid/section4buildlog.txt /home/walid/section5buildlog.txt /home/walid/section6buildlog.txt /home/walid/section7buildlog.txt /etc/apache2/sites-available/apps.vhost /etc/apache2/sites-available/default /etc/apache2/sites-available/ispconfig.conf /etc/apache2/sites-available/ispconfig.vhost /etc/apache2/apache2.conf /etc/apache2/ports.conf
 
sudo ls -lah /etc/apache2/ssl.key
sudo ls -lah /etc/apache2/ssl.csr
sudo ls -lah /etc/apache2/ssl.crt
sudo ls -lah /usr/local/lib/drush
sudo ls -lah /usr/local/lib/.drush
sudo ls -lah /var/www
sudo ls -lah ~/config

# alternative way to install drush
cd /usr/local/lib
sudo wget $DRUSH_GZ_URL
sudo tar zxvf $DRUSH_GZ_FILE
sudo chmod u+x drush
#sudo chown -cR $MYADMINUSR:$MYADMINUSR /usr/local/lib/drush
sudo chown -cR $MYADMINUSR /usr/local/lib/drush
sudo ln -s /usr/local/lib/drush/drush /usr/local/bin/drush
sudo ln -s /usr/local/lib/drush /root/drush

(echo y;) | sudo drush dl --destination=/usr/local/lib/.drush provision-6.x
sudo chmod u+x .drush
#sudo chown -cR $MYADMINUSR:$MYADMINUSR /usr/local/lib/.drush
sudo chown -cR $MYADMINUSR /usr/local/lib/.drush
sudo ln -s /usr/local/lib/.drush /root/.drush

sudo ln -s /usr/local/lib/drush /home/$MYADMINUSR/drush
sudo ln -s /usr/local/lib/.drush /home/$MYADMINUSR/.drush


##################################################################
fi
##################################################################

sudo /etc/init.d/apache2 restart
sudo /etc/init.d/mysql restart
sudo /etc/init.d/postfix restart
sudo /etc/init.d/saslauthd restart
sudo /etc/init.d/fail2ban restart
sudo /etc/init.d/webmin restart

echo "\$RUN_SECTION_1=$RUN_SECTION_1"
echo "\$RUN_SECTION_2=$RUN_SECTION_2"
echo "\$RUN_SECTION_3=$RUN_SECTION_3"
echo "\$RUN_SECTION_4=$RUN_SECTION_4"
echo "\$RUN_SECTION_5=$RUN_SECTION_5"
echo "\$RUN_SECTION_6=$RUN_SECTION_6"
echo "\$RUN_SECTION_7=$RUN_SECTION_7"

echo "\$RUN_AEGIR_INST=$RUN_AEGIR_INST"
echo "\$RUN_ISPC_INST=$RUN_ISPC_INST"
echo "\$ISPC_INST_MODE=$ISPC_INST_MODE"
echo "\$RUN_WEBMIN_INST=$RUN_WEBMIN_INST"

echo "\$ORIGINAL_HOSTNAME=$ORIGINAL_HOSTNAME"
echo "\$MYADMINUSR=$MYADMINUSR"
echo "\$MYSQLROOTPASS=$MYSQLROOTPASS"
echo "\$DRUSH_VERSION=$DRUSH_VERSION"
echo "\$DRUSH_MAKE_VERSION=$DRUSH_MAKE_VERSION"
echo "\$PROVISION_VERSION=$PROVISION_VERSION"
echo "\$QUICKSTART_VERSION=$QUICKSTART_VERSION"
echo "\$DRUSH_GZ_URL=$DRUSH_GZ_URL"
echo "\$DRUSH_GZ_FILE=$DRUSH_GZ_FILE"
echo "\$FAIL2BAN_URL=$FAIL2BAN_URL"
echo "\$FAIL2BAN_FILE=$FAIL2BAN_FILE"
echo "\$ISPC3_FILE_URL=$ISPC3_FILE_URL"
echo "\$ISPC3_FILE=$ISPC3_FILE"
echo "\$REAL_EMAIL=$REAL_EMAIL"
echo "\$MY_FULLHOSTNAME=$MY_FULLHOSTNAME"
echo "\$MY_FULLISPCHOSTNAME=$MY_FULLISPCHOSTNAME"
echo "\$MY_IPRANGE=$MY_IPRANGE"
echo "\$MY_IP=$MY_IP"
echo "\$HOST=$HOST"
echo "\$HOSTNAME=$HOSTNAME"
echo "\$ISPCHOSTNAME=ISPCHOSTNAME"

echo "about flocking time DONE"
echo "you started at "$RUN_STARTED
echo "you ended at"`date`

sudo rm ~/.nano_history
#sudo chown $MYADMINUSR:$MYADMINUSR ~/.rnd
sudo chown $MYADMINUSR ~/.rnd

sudo nano /etc/sudoers /etc/network/interfaces /etc/resolv.conf /etc/hosts /etc/hostname /etc/ssh/sshd_config /etc/aliases /etc/mysql/my.cnf /etc/postfix/master.cf /etc/phpmyadmin/config-db.php /etc/php5/apache2/conf.d/uploadprogress.ini /etc/postfix/sasl/smtpd.conf /etc/default/saslauthd /etc/courier/imapd.cnf /etc/courier/pop3d.cnf /etc/default/pure-ftpd-common /etc/fail2ban/jail.local /etc/fail2ban/action.d/route.conf /etc/fail2ban/filter.d/pureftpd.conf /etc/fail2ban/filter.d/courierpop3.conf /etc/fail2ban/filter.d/courierpop3s.conf /etc/fail2ban/filter.d/courierimap.conf /etc/fail2ban/filter.d/courierimaps.conf /etc/fail2ban/filter.d/roundcube.conf /usr/bin/fail2ban-client /usr/local/ispconfig/interface/lib/shelluser_blacklist /etc/webmin/miniserv.conf /home/$MYADMINUSR/section1buildlog.txt /home/$MYADMINUSR/section2buildlog.txt /home/$MYADMINUSR/section3buildlog.txt /home/$MYADMINUSR/section4buildlog.txt /home/$MYADMINUSR/section5buildlog.txt /home/$MYADMINUSR/section6buildlog.txt /home/$MYADMINUSR/section7buildlog.txt /etc/apache2/sites-available/apps.vhost /etc/apache2/sites-available/default /etc/apache2/sites-available/ispconfig.conf /etc/apache2/sites-available/ispconfig.vhost /etc/apache2/apache2.conf /etc/apache2/ports.conf


