# !/bin/bash
##################################################################
# PRE BUILD (1-prebuild.sh)
##################################################################
# Execute the following while logged in as root via counsole
# uncomment the following line to be prompted for a timezone configuration (useful for ensuring your build log echos dates in your local timezone)
    # dpkg-reconfigure tzdata
sudo test -e prebuildlog.txt && sudo rm -f prebuildlog.txt
echo "done at: "`date` | tee -a prebuildlog.txt
# INSTALL_ADMIN_USR creates an admin user. Some VPS servers don't have an admin user and only
# come with the root user but most VMs and disk based builds have an admin user. 
# Set INSTALL_ADMIN_USR = yes, and MYADMINUSR = youradminusername
# to create a new admin user, or add an admin user. The REST of the install runs as the
# MYADMINUSR, using sudo where needed.
INSTALL_ADMIN_USR=yes
MYADMINUSR=walid

MYSQLROOTPASS=walid
MY_HOSTNAME=walid
MY_DOMAIN=dev

# Common values for MY_ISPC_HOST are "cp" or "ispc" (sans quotes). This is the url
# for ISPConfig3's control panel
MY_ISPC_HOST=ispc

# ORIGINAL_HOSTNAME should be set to your server's original hostname
# This is needed to maintain some localized name resolution on some servers.
# For example on rackspace, when you crate a VPS, dedicated, or CloudVPS server
# you're prompted for a hostname, use that value here. See /etc/hostname
ORIGINAL_HOSTNAME=walid

# To maintain compatability with the Drupal project Quickstart, leave
# INSTALL_QUICKSTART_USR = yes. This will also let you follow QuickStart tutorials
# to enhance this server
INSTALL_QUICKSTART_USR=yes

MY_IPRANGE=10.10.0.0/16
MY_IP=10.10.0.50
MY_IPNETMASK=255.255.0.0
MY_IPNETWORK=10.10.0.0
MY_IPBROADCAST=10.10.255.255
MY_IPGATEWAY=10.10.0.1

MY_FULLHOSTNAME=$MY_HOSTNAME.$MY_DOMAIN
MY_FULLISPCHOSTNAME=$MY_ISPC_HOST.$MY_FULLHOSTNAME

# set vars for later use
export HOST=$MY_HOSTNAME
export HOSTNAME=$MY_FULLHOSTNAME
export ISPCHOSTNAME=$MY_FULLISPCHOSTNAME

# fixed, post step 3 /etc/hosts (doesn't work inline, must run post | validate)
# 127.0.0.1 points to walid.dev, should be localhost.localdomain localhost
echo $MY_FULLHOSTNAME | sudo tee /etc/hostname
sudo sed -i '/127\.0\.0\.1/d' /etc/hosts
sudo sed -i '/127\.0\.1\.1/d' /etc/hosts
sudo sed -i '/'$MY_IP'/d' /etc/hosts
sudo tee -a /etc/hosts <<EOFMARKER1600
    127.0.0.1 localhost.localdomain localhost $ORIGINAL_HOSTNAME
    $MY_IP aegir.$MY_FULLHOSTNAME $MY_FULLHOSTNAME $MY_FULLISPCHOSTNAME www.$MY_FULLHOSTNAME *.$MY_FULLHOSTNAME $MY_HOSTNAME
EOFMARKER1600
# sudo nano /etc/hosts

# (optional) set 50022 as the ssh port, if you comment Port 22, the server will only be
# accessible by port 50022 
sudo sed -i 's/Port 22/\# '$HOST' edits\: \
\# Port 22 \# original\
 Port 50022/g' /etc/ssh/sshd_config
# sudo /etc/init.d/ssh restart

# make it so that members of the sudo group don't have to enter their password
# when they sudo a command
sudo sed -i 's/%sudo/\#%sudo/g' /etc/sudoers
echo "
    %sudo ALL=(ALL) NOPASSWD: ALL
    # $MYADMINUSR ALL=(ALL) NOPASSWD: ALL
    # quickstart ALL=(ALL) NOPASSWD: ALL" | sudo tee -a /etc/sudoers
sudo visudo -c
 # echo "quickstart ALL=(ALL) NOPASSWD: ALL" | sudo tee -a /etc/sudoers > /dev/null
##################################################################
if [ "$INSTALL_ADMIN_USR" == "yes" ]; then 
##################################################################
# create admin user
(echo $MYSQLROOTPASS; echo $MYSQLROOTPASS; echo $MYADMINUSR; echo; echo; echo; echo; echo y;) | sudo adduser --home /home/$MYADMINUSR --shell /bin/bash --uid 1000 $MYADMINUSR
sudo usermod -G adm --append $MYADMINUSR
sudo usermod -G dialout --append $MYADMINUSR
sudo usermod -G cdrom --append $MYADMINUSR
sudo usermod -G sudo --append $MYADMINUSR
sudo usermod -G www-data --append $MYADMINUSR
sudo usermod -G plugdev --append $MYADMINUSR
sudo usermod -G lpadmin --append $MYADMINUSR
sudo usermod -G sambashare --append $MYADMINUSR
sudo usermod -G admin --append $MYADMINUSR
sudo usermod -G ssh --append $MYADMINUSR
sudo usermod -G users --append $MYADMINUSR
sudo usermod -G root --append $MYADMINUSR
sudo usermod -G sys --append $MYADMINUSR
sudo usermod -G drushusers --append $MYADMINUSR
sudo sed -i 's/# '$MYADMINUSR'/'$MYADMINUSR'/g' /etc/sudoers
sudo visudo -c

##################################################################
fi
##################################################################



##################################################################
if [ "$INSTALL_QUICKSTART_USR" == "yes" ]; then 
##################################################################
# create quick start user (remove if you don't want this user created)
(echo quickstart; echo quickstart; echo QUICKSTART; echo; echo; echo; echo; echo y;) | sudo adduser --home /home/quickstart --shell /bin/bash --uid 1001 quickstart
sudo usermod -G adm --append quickstart
sudo usermod -G dialout --append quickstart
sudo usermod -G cdrom --append quickstart
sudo usermod -G sudo --append quickstart
sudo usermod -G www-data --append quickstart
sudo usermod -G plugdev --append quickstart
sudo usermod -G lpadmin --append quickstart
sudo usermod -G sambashare --append quickstart
sudo usermod -G admin --append quickstart
sudo usermod -G ssh --append quickstart
sudo usermod -G aegir --append quickstart
sudo usermod -G ispapps --append quickstart
sudo usermod -G ispconfig --append quickstart
sudo usermod -G users --append quickstart
sudo sed -i 's/# quickstart/quickstart/g' /etc/sudoers
sudo visudo -c

##################################################################
fi
##################################################################

# Install some build reqs
sudo apt-get -y install ssh openssh-server debconf-utils

# Update 
sudo apt-get update
sudo apt-get -y upgrade

# make done marker/log
echo "done at: "`date` | tee -a prebuildlog.txt

##################################################################
if [ "$INSTALL_ADMIN_USR" == "yes" ]; then 
##################################################################
# copy to admin user
sudo cp prebuildlog.txt /home/$MYADMINUSR/prebuildlog.txt
sudo cp 1-prebuild.sh /home/$MYADMINUSR/1-prebuild.sh
sudo chown $MYADMINUSR /home/$MYADMINUSR/prebuildlog.txt
sudo chown $MYADMINUSR /home/$MYADMINUSR/1-prebuild.sh

##################################################################
fi
##################################################################

sudo nano /etc/network/interfaces /etc/group /etc/passwd /etc/sudoers /etc/ssh/sshd_config /etc/resolv.conf /etc/hosts /etc/hostname /home/$MYADMINUSR/1-prebuild.sh /home/$MYADMINUSR/prebuildlog.txt

# reboot
sudo reboot 30

# When you're done, reboot, login via your new MYADMINUSR user, 