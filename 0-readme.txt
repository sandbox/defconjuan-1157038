(c) 2012, Beget LLC. Author: Robert Hernandez.

Do the following on a fresh VM or physical Ubuntu 12.04 build while logged in as root over ssh or from the console.

-------------------------------------------------------------
PREBUILD
-------------------------------------------------------------
# Install git
apt-get install -y git

# Clone git repo & move into new repo
git clone --branch master http://git.drupal.org/sandbox/defconjuan/1157038.git walid && cd walid

# Open 1-prebuild.sh. Edit variables in header or your system will likely become unreachable .
nano 1-prebuild.sh

# Execute 1-prebuild.sh script
/bin/bash 1-prebuild.sh

	# OR, for the nerd here's a one line command that will give you a chance to edit the 1-prebuild.sh file before it is executed. 
	apt-get install -y git && git clone git clone --branch master http://git.drupal.org/sandbox/defconjuan/1157038.git walid && cd walid && nano 1-prebuild.sh && /bin/bash 1-prebuild.sh

# After running, machine will open several windows with edited config files for your review before the system reboots

-------------------------------------------------------------
BUILD
-------------------------------------------------------------
# After reboot login over ssh or from terminal as root, switch to admin user
su - walid

# Clone repo into admin user & move into new repo
git clone --branch master http://git.drupal.org/sandbox/defconjuan/1157038.git walid && cd walid

# Open 2-build.sh. Edit variables in header of your system will likely become unreachable. 
nano 2-build.sh

# Execute 2-build.sh script.
/bin/bash 2-build.sh

	# OR, for the nerd:
		su - walid
		# here's a one line command that will give you a chance to edit the 2-ebuild.sh file before it is executed. 
		git clone --branch master http://git.drupal.org/sandbox/defconjuan/1157038.git walid && cd walid && nano 2-build.sh && /bin/bash 2-build.sh

# After running, machine will open several windows with edited config files for your review. 

-------------------------------------------------------------
NOTES
-------------------------------------------------------------
# When you confirm all is well, reboot.
	# ISPConfig Control Panel
		# Visit. https://[yourserver]:8443
		# Login with username: admin, and password: admin. 
		# Go to Tools > User Settings > Password and Language and set your password to: walid. 
	# Webmin Control Panel
		# https://[yourserver]:50000
		# Username: root Password: walid
	# SSH runs on Port 50022
		# Usernames: root, walid, quickstart
		# Password: walid
	# phpMyAdmin
		# https://[yourserver]:8443/phpmyadmin
		# Usernames: root, walid, quickstart
		# Password: walid
	# SquirrelMail
		# https://[yourserver]:8443/squirrelmail
		# Usernames: [your@email.com]
		# Password: [Password entered at account setup]
	# Use ISPConfig to create all new site/domain/webdev related shell, sftp, and mail accounts; and apache websites
	# Use Webmin to set your network configs based on your setup (or if you've moved your machine), to run updates, and for all high-end admin (but don't use it to manage ISPConfig created assets.)
	# Use phpMyAdmin for dumping and importing databases
	# For status and logging run >/bin/bash mytail
	# Htop is installed, run >htop
